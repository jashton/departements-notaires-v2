#!/usr/bin/env groovy

pipeline {
  agent any
  stages {
    stage('App environment') {
      agent {
        dockerfile {
          filename 'Dockerfile'
          dir './docker/lamp'
          additionalBuildArgs  '--build-arg PHP_VERSION=7.2'
          args '-u root --privileged -v "/var/lib/jenkins/composer/auth.json:/home/.composer/auth.json"'
        }
      }
      stages {
        stage('Build') {
          environment {
            YARN_CACHE_FOLDER = "${env.WORKSPACE}/.cache/yarn/v6"
          }
          steps {
            sh 'sed -i "s#/var/www/html#${WORKSPACE}/appli_sf/public#" /etc/apache2/sites-available/000-default.conf'
            sh '/run.sh'
            sh 'cp .env.dist .env'

            dir('appli_sf') {
              sh 'cp /etc/hosts ~/hosts.new'
              sh 'sed -i "s/127.0.0.1.*/127.0.0.1 localhost database/" ~/hosts.new'
              sh 'cp -f ~/hosts.new /etc/hosts'
              sh 'cp .env.dist .env'
              sh "composer config cache-dir '${env.WORKSPACE}/.cache/.composer'"
              sh 'composer install --no-progress --no-suggest --no-interaction --no-scripts'
              sh 'yarn && yarn run build'
              sh 'bin/console doctrine:migration:migrate --no-interaction'
              sh 'bin/console cache:clear'
              sh 'chown -R www-data: .'
            }
          }
        }
        stage("Symfony linters") {
          steps {
            dir('appli_sf') {
              sh 'bin/console lint:yaml --no-interaction config'
              sh 'bin/console lint:yaml --no-interaction translations'
            }
          }
        }
        stage("Code Sniffer") {
          steps {
            dir('appli_sf') {
              sh './vendor/bin/phpcs'
            }
          }
        }
        stage("PHP Lint") {
          steps {
            dir('appli_sf') {
              sh './vendor/bin/phplint --no-progress --no-interaction'
            }
          }
        }
        stage("PHPStan") {
          steps {
            dir('appli_sf') {
              sh './vendor/bin/phpstan analyse --no-progress --no-interaction'
            }
          }
        }
        stage("Unit tests") {
          steps {
            dir('appli_sf') {
              sh 'bin/phpunit tests/UnitTests'
            }
          }
        }
        stage("Functional tests") {
          steps {
            dir('appli_sf') {
              sh 'bin/phpunit tests/FunctionalTests'
            }
          }
        }
      }
    }
  }
  post {
    always {
      sh 'git checkout -- .'
      sh "git clean -fdx --exclude='.cache'"
    }
  }
}

