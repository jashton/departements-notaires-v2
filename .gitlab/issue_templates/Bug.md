## Etapes pour reproduire le bug

(Comment une tierce personne peut reproduire le bug - ceci est très important)

## Quel est le comportement *actuel* du bug

(Le comportement actuellement observé)

## Quel est le comportement *correct*

(Le comportement attendu à la place)

## Fichiers journaux ou copies d'écrans

(Coller les fichiers journaux pertinents. Merci d'utiliser les bloc de code (```)
pour former les journaux, très difficile à lire sinon.)

## Pistes de solution

(Si vous pouvez, un lien vers le code présumé responsable du problème)

/label ~Bug
/cc @mfaure
