![](Documentation/Images/Logo-Departements-Notaires.jpg)

# Départements & Notaires v2

**Départements et Notaires** est un extranet permettant d'apporter une réponse en temps réel aux études notariales 
chargées d’une succession s'interrogeant sur l'existence éventuelle d'une créance du Département au titre de l'aide sociale.

**Départements et Notaires** a été initialement conçu et développé par le Département du Rhône en lien avec la Chambre 
des Notaires du Rhône, et déployé la première fois en janvier 2016 sous le nom **Rhône + Notaires**.

## Fonctionnalités

* **Pour les notaires** :
  * Recherche et réponse immédiate, à l'écran et confirmation par email avec lettre jointe PDF
  * Historique de leurs propres recherches
* **Pour les gestionnaires** :
  * Historiques de toutes les recherches réalisées
  * Statistiques

## Installation et Documentation

* [Installation](Documentation/Operateur/installation.md)
* [Import d'un jeu de test](Documentation/Utilisateur/Import-individus.md) et [exemples de recherches types
](Documentation/Utilisateur/Import-individus-test-verifications.md)
* Grands principes de [l'algorithme de recherche](Documentation/Utilisateur/Algorithme_de_recherche.md)
* [Personnalisation de l'application](Documentation/Utilisateur/Personnalisation.md)
* Documentation fonctionnelle **TODO**

Mises à jour :

* [Mise à jour v2.0.0-beta.1 vers v2.0.0-beta.2](Documentation/Operateur/Mise-a-jour-v2.0.0-beta1-vers-v2.0.0-beta2.md)
* [Mise à jour v2.0.0-beta.2 vers v2.0.0-beta.3](Documentation/Operateur/Mise-a-jour-v2.0.0-beta.2-vers-v2.0.0-beta.3.md)
* [Mise à jour v2.0.0-beta.3 vers v2.0.0-beta.4](Documentation/Operateur/Mise-a-jour-v2.0.0-beta.3-vers-v2.0.0-beta.4.md)

Pour développeurs : 

* Construction des [images Docker](Documentation/Developpeur/installation_docker.md)
* [Tests end-to-end](Documentation/Developpeur/TEST_E2E_D&N_v2.md) pour les recettes
 
## Changelog

[Changelog](CHANGELOG.md)
