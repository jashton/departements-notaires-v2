# Migration v2.0.0-beta.3 vers v2.0.0-beta.4

Sont décrites ici les étapes pour migrer l'application v2.0.0-beta.3 vers v2.0.0-beta.4

## Effacer les fichiers générés (sauf les médias et ceux pour la personnalisation) dans le dossier de l'application

```shell
    rm -rf app/assets
    rm -rf app/config
    rm -rf app/public/build
    rm -rf app/public/bundles
    rm -rf app/src
    rm -rf app/templates/default
    rm -rf app/tests
    rm -rf app/translations/messages.fr.yaml
    rm -rf app/vendor
```

## Récupérer l'application à jour

* Télécharger Départements & Notaires v2.0.0-beta.4
* Dézipper le fichier dans le dossier de l'application

Attention : Si vous utilisez un fichier de CSS personnalisé (`app/public/styles.css`), pensez à le récupérer AVANT de dézipper le fichier dans le dossier de l'application. Vous pourrez ensuite copier votre fichier en lieu et place du fichier vide contenu dans l'archive.

## Vider les caches

```shell
./bin/console cache:clear
```

## Mettre à jour la base de données

```shell
./bin/console doctrine:migrations:migrate
```
