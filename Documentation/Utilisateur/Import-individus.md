# Import des données CSV de test

Sont décrites ici les étapes pour importer des données dans l'application via un fichier CSV.

Un fichier CSV comprenant des données d'exemples est disponible : [individus.csv](../../appli_sf/imports/individus.csv)

L'import permet de remplir la table `person` de l'application (sur laquelle est effectuée la recherche), cette table regroupe les demandes suivantes :

 - Tous les individus bénéficiant, ou ayant bénéficié, de prestations d'ASG (Aide Social Générale),
   d'ADPA (Allocation Départementale Personnalisée d'Autonomie) ou de PCH (Prestation de Compensation d'Handicap)
 - Tous les individus dont la ou les demandes d'ASG (Aide Social Générale), d'ADPA (Allocation
   Départementale Personnalisée d'Autonomie) ou de PCH (Prestation de Compensation d'Handicap) sont en cours d'instruction.

Pour information, par rapport à la V1, cette table est la fusion des tables `demandes` et `individus`.

# Procédure d'import

Déposer le fichier à importer dans le dossier `imports` à la racine du projet.
Lancer la commande suivante (où "fichier.csv" correspond au nom du fichier à importer) :

    bin/console import:individus "imports/fichier.csv"

Un import supprime entièrement les précédentes données importées. Il faut importer l'intégralité des données à chaque nouvel import.

# Détail du fichier CSV

Fichier CSV au format suivant : 

* Séparateur : `,`
* Délimitation des chaines de caractères : `"`

Les différentes colonnes :

* `num_ind` : numéro individu issu de l'application tiers, propre à elle : int(15)
* `sexe` : sexe de la personne (F ou M) : varchar(1)
* `nom_usage` : nom d'usage (marital) de la personne : varchar(250)
* `nom_civil` : nom de naissance de la personne : varchar(250)
* `prenom` : premier prénom de la personne : varchar(250)
* `prenomd` : second prénom de la personne : varchar(250)
* `prenomt` : troisième prénom de la personne : varchar(250)
* `date_naissance` : date de naissance de la personne :  date(jj/mm/aaaa)
* `adresse` : adresse du référent administratif du dossier de la personne : varchar(250)
* `mdr` : nom du référent administratif du dossier de la personne : varchar(250)
* `telephone` : téléphone du référent administratif : varchar(250)
* `mail_mdr` : mail du référent administratif : varchar(250)
* `libelle` : aide accordée à la personne (exemple : Charges exceptionnelles (mensuel), Fauteuil roulant (FDCH), Aides techniques (FDCH), ADPA à domicile, Aide ménagère couple PH, etc) : varchar(250)
* `code` : ode indiquant si l'aide est récupérable ou non (1SEXTREC : recupérable, 1SEXTNONR : non récupérable) : varchar(50)

Important : Il faut bien laisser la première ligne d'entête comme dans le fichier d'exemple.

