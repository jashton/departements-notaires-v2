# Présentation de l'algorithme de recherche

La recherche constituant une des fonctionnalités essentielles de Départements & Notaires, il est important d'en
 comprendre le fonctionnement.

## Recherche et contexte : un duo inséparable

La recherche porte sur le contenu de la base de donnée à un instant t, que nous appelons contexte.

Il est important de comprendre qu'une même recherche peut donner des résultats différents selon la présence (ou non) 
d'éventuels homonymes. De même si la base de donnée évolue avec le temps (avec l'apparition d'une nouvelle aide par
exemple, et donc de nouvelles personnes), il se peut qu'une même recherche ne donne plus exactement les mêmes 
résultats qu'auparavant.

Cette précaution méthodologique étant posée, passons à l'algorithme lui-même.

## Étapes de l'algorithme de recherche de Départements & Notaires

La recherche d'une personne s'effectue en plusieurs étapes.

## Étape 1

La recherche est lancée avec les conditions suivantes : 

* `code_aide` est non null
* ET correspondance exacte de la date de naissance
* ET
    * SOIT 
        * correspondance du nom, moyennant tiret (un éventuel tiret étant remplacé par un espace) 
        * ET correspondance exacte d'un des 3 prénoms
    * SOIT 
        * correspondance exacte du nom
        * ET correspondance des 3 premières lettres du premier prénom

Le nombre de réponses renvoyées (N) détermine les actions à prendre :

* N > 1 : le résultat est **ambigu**, la recherche est terminée.
* N = 1 : le caractère récupérable ou non de l'aide est remonté, la recherche est terminée.
* N = 0 : on passe à l'étape 2

## Étape 2

La requête de l'étape 1 est renvoyée, mais la date de naissance est réduite à l'année (le jour et le mois ne sont pas 
considérés)

Le nombre de réponses renvoyées (N) détermine les actions à prendre :

* N >= 1 : le résultat est **ambigu**, la recherche est terminée.
* sinon : on passe à l'étape 3

## Étape 3

La requête de l'étape 1 est renvoyée (la date de naissance est exacte), mais sans aucun prénom

Le nombre de réponses renvoyées (N) détermine les actions à prendre :

* N >= 1 : le résultat est **ambigu**, la recherche est terminée.
* sinon : le résultat est **inconnu**, la recherche est terminée.

## Recherche Soundex

Références techniques :

* [Documentation MariaDB Soundex](https://mariadb.com/kb/en/soundex/)
* Cas d'usage [Soundex sur sql.sh](https://sql.sh/fonctions/soundex)

**Remarque importante** : la fonctionnalité Soundex est construite pour la langue anglaise. La documentation
 officielle de MariaDB annonce que son usage sur d'autres langues peut amener à des résultats non fiables. L
 'utilisateur de Départements & Notaires doit avoir pris de connaissance de ce fait avant l'activation de l'option.

La recherche Soundex d'une personne s'effectue exatement de la même manière qu'une recherche "classique".
Seules les conditions de l'étape 1 sont différentes (conditions utilisées pour les 3 étapes)

### Étape 1 - Soundex

La recherche est lancée avec les conditions suivantes : 

* `code_aide` est non null
* ET correspondance exacte de la date de naissance
* ET
    * SOIT 
        * correspondance Soundex du nom
        * ET correspondance Soundex d'un des 3 prénoms
    * SOIT 
        * correspondance Soundex du nom
        * ET correspondance des 3 premières lettres du premier prénom


# Exemples de cheminements de recherche

## Recherche exacte

Vous trouverez ci-dessous le cheminement de la recherche suivante :
    
   * Date de décès : 10/02/2020
   * Lieu de décès : VILLEFRANCHE
   * Date de l'acte de décès : 12/02/2020
   * Prénom : Aimé
   * Nom d'usage : OLMO
   * Date de naissance : 06/12/1977
   
 En base de données est présent :
 
 | NOM | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
 |----------|:-------------:|:-------------:|:-------------:|:-------------:|------:|
 | OLMO | Aimé | | | 06/12/1977 | 1SEXTREC |

- Etape 1 : La recherche suivante est lancée sur la base de données (conditions ET) :
```
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```
=> 1 seul résultat trouvé, la Personne est **connue**

## Recherche avec prénom inexacte

Vous trouverez ci-dessous le cheminement de la recherche suivante :
    
   * Date de décès : 10/02/2020
   * Lieu de décès : VILLEFRANCHE
   * Date de l'acte de décès : 12/02/2020
   * Prénom : A
   * Nom d'usage : OLMO
   * Date de naissance : 06/12/1977

 En base de données est présent :
 
 | NOM | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
 |----------|:-------------:|:-------------:|:-------------:|:-------------:|------:|
 | OLMO | Aimé | | | 06/12/1977 | 1SEXTREC |

- Etape 1 : La recherche suivante est lancée sur la base de données (conditions ET) :
```
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1977'
NOM = 'OLMO'
(PRENOM1 = 'A' || PRENOM2 = 'A' || PRENOM3 = 'A') OU (PRENOM1 LIKE 'A%')
```
=> 1 seul résultat trouvé, la Personne est **connue**

## Recherche avec plusieurs prénoms

Vous trouverez ci-dessous le cheminement de la recherche suivante :
    
   * Date de décès : 10/02/2020
   * Lieu de décès : VILLEFRANCHE
   * Date de l'acte de décès : 12/02/2020
   * Prénom : Mickaël
   * Prénom 2 : Aimé
   * Nom d'usage : OLMO
   * Date de naissance : 06/12/1977

 En base de données est présent :
 
 | NOM | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
 |----------|:-------------:|:-------------:|:-------------:|:-------------:|------:|
 | OLMO | Aimé | | | 06/12/1977 | 1SEXTREC |

- Etape 1 : La recherche suivante est lancée sur la base de données (conditions ET) :
```
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1977'
NOM = 'OLMO'
(PRENOM1 = 'Mickaël' || PRENOM2 = 'Mickaël' || PRENOM3 = 'Mickaël') OU (PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU  (PRENOM1 LIKE 'Mic%')
```
=> 1 seul résultat trouvé, la Personne est **connue**

## Recherche avec plusieurs résultats

Vous trouverez ci-dessous le cheminement de la recherche suivante :
    
   * Date de décès : 10/05/2020
   * Lieu de décès : GUINGAMP
   * Date de l'acte de décès : 12/05/2020
   * Prénom : Louis
   * Nom d'usage : DURAND
   * Date de naissance : 03/01/1911

 En base de données est présent :
 
 | NOM | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
 |----------|:-------------:|:-------------:|:-------------:|:-------------:|------:|
 | DURAND | Louis | | | 03/01/1911 | 1SEXTREC |
 | DURAND | Louise | | | 03/01/1911 | 1SEXTREC |

- Etape 1 : La recherche suivante est lancée sur la base de données (conditions ET) :
```
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '03/01/1911'
NOM = 'DURAND'
(PRENOM1 = 'Louis' || PRENOM2 = 'Louis' || PRENOM3 = 'Louis') OU (PRENOM1 LIKE 'Lou%')
```
=> 2 résultats trouvés, recherche **ambigue**

## Recherche date de naissance inexacte, résultat ambigu

Vous trouverez ci-dessous le cheminement de la recherche suivante :
    
   * Date de décès : 10/02/2020
   * Lieu de décès : VILLEFRANCHE
   * Date de l'acte de décès : 12/02/2020
   * Prénom : Aimé
   * Nom d'usage : OLMO
   * Date de naissance : 07/08/1977

 En base de données est présent :
 
 | NOM | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
 |----------|:-------------:|:-------------:|:-------------:|:-------------:|------:|
 | OLMO | Aimé | | | 06/12/1977 | 1SEXTREC |

- Etape 1 : La recherche suivante est lancée sur la base de données (conditions ET) :
```
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '07/08/1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```
=> aucun résultat trouvé

- Etape 2 : La recherche suivante est ensuite lancée sur la base de données (conditions ET) :
```
CODE_AIDE != NULL 
DATE_DE_NAISSANCE = '1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```
=> 1 résultat trouvé, recherche **ambigue**

## Recherche non trouvée

Vous trouverez ci-dessous le cheminement de la recherche suivante :
    
   * Date de décès : 10/02/2020
   * Lieu de décès : VILLEFRANCHE
   * Date de l'acte de décès : 12/02/2020
   * Prénom : Mickaël
   * Nom d'usage : DUPONT
   * Date de naissance : 07/08/1986

- Etape 1 : La recherche suivante est lancée sur la base de données (conditions ET) :
```
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '07/08/1986'
NOM = 'DUPONT'
(PRENOM1 = 'Mickaël' || PRENOM2 = 'Mickaël' || PRENOM3 = 'Mickaël') OU (PRENOM1 LIKE 'Mic%')
```
=> aucun résultat trouvé

- Etape 2 : La recherche suivante est ensuite lancée sur la base de données (conditions ET) :
```
CODE_AIDE != NULL 
DATE_DE_NAISSANCE = '1986'
NOM = 'DUPONT'
(PRENOM1 = 'Mickaël' || PRENOM2 = 'Mickaël' || PRENOM3 = 'Mickaël') OU (PRENOM1 LIKE 'Mic%')
```
=> aucun résultat trouvé

- Etape 3 : La recherche suivante est ensuite lancée sur la base de données (conditions ET) :
```
CODE_AIDE != NULL 
DATE_DE_NAISSANCE = '07/08/1986'
NOM = 'DUPONT'
```

=> aucun résultat trouvé, la personne est **inconnue**
