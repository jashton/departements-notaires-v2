# Symfony Docker

## Download

[Download](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/repository/archive.zip?ref=master)

## Install

    make                    # self documented makefile

    make install            # build, launch, install, compile (the 5 next)

    make droits             # Add execution on bin files
    make docker-build       # build containers
    make docker-start       # launch containers
    make composer-install   # install vendors
    make assets             # compile assets

Set environment variables :

    ./bin/console cache:warmup


## Start project

    make install
    ./bin/cc.sh


## Use

Symfony application : ```http://notaires.docker.localhost:8020``` or ```http://127.0.0.1:8020```

pgAdmin : ```http://notaires.docker.localhost:5050```

Email client : ```http://notaires.docker.localhost:8025```


## Commands

Symfony command line :

    ./bin/console

Generate assets :

    ./bin/console assets:install --symlink
    make assets

Clear caches :

    ./bin/console cache:clear

Script to clear all caches and generate assets :

    ./bin/cc.sh


## Add a new server in PgAdmin

Host name/address : ```database``` (in PhpStorm, use ```localhost```)

Port : ```5432```

Database name : ```notaires```

Username : ```notaires```

Password : ```password```


## Developper tools

### Pre-commit hook with PHP Code Sniffer

Install pre-commit with ```./bin/composer copyHooks```

Now, PHP Code Sniffer analyse your code before each commit.

To remove pre-commit validations, delete file ```.git/hooks/pre-commit```

### PHP_CodeSniffer

Inspecting the PHP code to detect violations of a set of defined encoding rules.

use with : ```./bin/phpcs```
or
```./bin/composer phpcs```

### PHP-CS-Fixer

Automatically analyze and correct the PHP code to comply with coding standards (PSR-2)

use with : ```./bin/php-cs-fixer fix```
or
```./bin/composer codestyle-fix```

### PHPLint

Checking and validating the syntax of the PHP code.

use with : ```./bin/phplint```
or
```./bin/composer phplint```

### PHPStan

PHPStan focuses on finding errors in your code without actually running it.
It catches whole classes of bugs even before you write tests for the code.
It moves PHP closer to compiled languages in the sense that the correctness of each line of the code can be checked before you run the actual line.

use with : ```./bin/phpstan```
or
```./bin/composer phpstan```

### Tests

#### PHPUnit

Launches unit and functional tests.

use with : ```./bin/phpunit```