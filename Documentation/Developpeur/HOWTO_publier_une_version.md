# Créer une version de Départements & Notaires

* Modifier le numéro de version dans `appli_sf/composer.json`, entrée `version`
* Tagguer la branche master (le build associé au tag comportera l'archive nommée avec le bon numéro de version)