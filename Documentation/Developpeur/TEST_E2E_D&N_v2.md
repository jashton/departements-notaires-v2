# TEST_E2E_D&N_v2.md

## 1. Installation 

* Installation avec Vagrant
* Création d'un utilisateur de type admin

## 2. Ajout d'un logo

1. Se connecter en tant qu'admin
1. Se rendre dans Administration > Paramètres de l'application
1. Saisir le nom de l'application : "D&N" 
1. Saisir le logo de l'application : choisir le fichier `Documentation/Images/Logo-Departements-Notaires.jpg`
1. Valider

Résultat attendu : le logo doit apparaître en haut à droite de l'écran, sous l'identification et au-dessus de la
 barre de navigation

## 3. Recherche sur base vide

1. Se connecter en tant qu'admin
1. Se rendre dans Recherche
1. Saisir les valeurs suivantes :
    * Cocher la case 'être en possession de l'acte de décès
    * Date de décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date de l'acte de décès : 12/02/2020
    * Prénom : Aimé
    * Nom d'usage : Olmo
    * Date de naissance : 12/06/1977

Résultat attendu : message la personne est inconnue.

## 4. Import de données

1. Depuis la machine Vagrant, créer le fichier `/var/www/notaires/imports/individus.csv`
1. Y copier/coller le contenu de `appli_sf/imports/individus.csv`
1. `cd /var/www/notaires/`
1. Selon le paramétrage de l'appli :
    1. Nom d'usage obligatoire : `bin/console import:individus "imports/individus.csv"`
    1. Nom civil obligatoire : `bin/console import:individus "imports/individus.csv" --use-civil-name=1`

Résultat attendu : 

```
Fin vérification de l'entête
 773/773 [============================] 100%
Fin import
```

## 5. Recherche 

Jouer les tests décrits dans [Import-individus-test-verifications.md](../Utilisateur/Import-individus-test-verifications.md).

## 6. Création d'un utilisateur de profil Notaire

1. Se connecter en tant qu'admin
1. Se rendre dans Administration > Gérer les utilisateurs
1. Cliquer sur le bouton *Ajouter un utilisateur*
1. Créer l'utilisateur avec les données suivantes :
    * Profil : étude notariale
    * Code CRPCEN : 1
    * E-mail : etude@notaire.fr
    * Description : étude
    * Adresse : Place de la République
    * Complément d'adresse : 2° étage
    * Code postal : 34000
    * Ville : Montpellier
1. Valider

Résultat attendu : 

* Dans la liste des utilisateurs, retrouver une ligne contenu `etude@notaire.fr` ET étant de profil *Etude notariale*

## 7. Instructeurs

Pour toutes les étapes suivantes se connecter en tant qu'admin

### 7.1 Création d'un instructeur
#### Etape 1
1. Se rendre dans Administration > Gérer les instructeurs
1. Cliquer sur le bouton *Ajouter un instructeur*
1. Créer l'instructeur avec les données suivantes :
    * Prénom : instructeur
    * Nom : AMBIGU
    * E-mail : instructeur1@notaire.fr
    * Téléphone : 06 06 06 06 06
    * Type de réponse : Ambigu
    * Initiales : A-Z
1. Cliquer sur le bouton *Ajouter*

Résultat attendu : 

#### Etape 2
* Dans la liste des instructeurs, retrouver une ligne contenant `instructeur1@notaire.fr` ET type de réponse *Ambigu* ET Initiales *A-Z*

1. Se rendre dans Recherche
1. Effectuer une recherche de type "ambigue" :
1. Saisir les valeurs suivantes :
    * Date de décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date de l'acte de décès : 12/02/2020
    * Prénom : Clara
    * Nom d'usage : BOURDIAU
    * Date de naissance : 01/09/1966

Résultat attendu :
 
* Vérifier que l'email à bien été envoyé à l'adresse `instructeur1@notaire.fr`

### 7.2 Création d'un second instructeur
#### Etape 1
1. Se rendre dans Administration > Gérer les instructeurs
1. Cliquer sur le bouton *Ajouter un instructeur*
1. Créer l'instructeur avec les données suivantes :
    * Prénom : instructeur
    * Nom : RECUPERATION
    * E-mail : instructeur@notaire.fr
    * Téléphone : 06 06 06 06 06
    * Type de réponse : Récupération
    * Initiales : A-Z
1. Cliquer sur le bouton *Ajouter*

Résultat attendu : 

* Dans la liste des instructeurs, retrouver une ligne contenant `instructeur2@notaire.fr` ET type de réponse *Récupération* ET Initiales *A-Z*
#### Etape 2
1. Se rendre dans Recherche
1. Effectuer une recherche de type "récupération" :
1. Saisir les valeurs suivantes :
    * Date de décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date de l'acte de décès : 12/02/2020
    * Prénom : Aimé
    * Nom d'usage : OLMO
    * Date de naissance : 06/12/1977

Résultat attendu :
 
* Vérifier que l'email à bien été envoyé à l'adresse `instructeur2@notaire.fr`

### 7.3 Création d'un instructeur multi-réponses et initiales communes avec un autre instructeur
#### Etape 1
1. Se rendre dans Administration > Gérer les instructeurs
1. Cliquer sur le bouton *Ajouter un instructeur*
1. Créer l'instructeur avec les données suivantes :
    * Prénom : instructeur
    * Nom : AMBIGU2
    * E-mail : instructeur3@notaire.fr
    * Téléphone : 06 06 06 06 06
    * Type de réponse : Ambigu, Récupération
    * Initiales : M-Z
1. Cliquer sur le bouton *Ajouter*

Résultat attendu : 

* Dans la liste des instructeurs, retrouver une ligne contenant `instructeur3@notaire.fr` ET type de réponse *Ambigu, Récupération* ET Initiales *M-Z*
#### Etape 2
1. Se rendre dans Recherche
1. Effectuer une recherche de type "ambigue" :
1. Saisir les valeurs suivantes :
    * Date de décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date de l'acte de décès : 12/02/2020
    * Prénom : Clara
    * Nom d'usage : BOURDIAU
    * Date de naissance : 01/09/1966

Résultat attendu :
 
* Vérifier que l'email à bien été envoyé à l'adresse `instructeur1@notaire.fr`
* Vérifier que l'email n'a pas été envoyé à l'adresse `instructeur3@notaire.fr`

## 8. Lien symbolique sur le dossier uploads

1. Se positionner dans le dossier public de l'application
1. Déplacer le dossier uploads dans /opt par exemple (ne fonctionne pas dans /tmp)
1. Faire un lien symbolique dans le dossier public vers le dossier uploads : `ln -s /opt/uploads uploads`

Résultat attendu : 

* Le logo s'affiche toujours dans l'entête et la page de connexion
* Les prévisus des images s'affichent bien dans les paramètres de l'application

## 9. CSS personnalisée

1. Se positionner dans le dossier public de l'application
1. Editer le fichier styles.css
1. Retirer les commentaires /* */ autour des classes CSS

Résultat attendu :

* Le rendu graphique de l'application est différent :
  - La couleur du texte
  - La couleur de fond
  - La couleur des titres
  - La couleur des boutons
  - l'image du bandeau est remplacée par un fond uni
  - les boutons de l'entete sont déplacés à gauche

## 10. Création d'un template et changement de template

1. Créer un nouveau template en suivant les étapes décrites ici : [Personnalisation.md](../Utilisateur/Personnalisation.md)
1. Paramétrer le nouveau template créé

Résultat attendu : 

* Après rechargement de la page, le nouveau template est bien pris en compte

## 11. Consultation des statistiques

1. Se connecter à l'application en tant qu'administrateur ou agent du département
1. Se rendre dans Statistiques

Résultat attendu :

* Les statistiques des 5 derniers mois s'affichent

1. Vérifier les nombres présents dans la première colonne dans les lignes *Nombre de réponses* et *Nombre de réponses : Récupérables*
1. Se rendre dans Recherche
1. Effectuer une recherche de type "récupération" :
1. Saisir les valeurs suivantes :
    * Date de décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date de l'acte de décès : 12/02/2020
    * Prénom : Aimé
    * Nom d'usage : OLMO
    * Date de naissance : 06/12/1977
 1. Se rendre dans Statistiques
 1. Vérifier les nombres présents dans la première colonne dans les lignes *Nombre de réponses* et *Nombre de réponses : Récupérables*
 
 Résultat attendu :
 
 * Les nombres présents dans la première colonne dans les lignes *Nombre de réponses* et *Nombre de réponses : Récupérables* ont augmenté de 1

## 12. Génération PDF des statistiques

1. Se connecter en tant qu'admin
1. Se rendre dans Statistiques
1. Sélectionner une date de début et une date de fin
1. Cliquer sur générer un PDF

Résultat attendu :

* Un PDF avec les statistiques sur la période choisie

## 13. Consultation historique des recherches

1. Se connecter à l'application
1. Se rendre dans Liste des recherches
1. Sélectionner les filtres suivants :
    * Type de réponse "Récupération"

Résultat attendu :

* La liste de toutes les recherches "Récupération" s'affichent

1. Se rendre dans Recherche
1. Effectuer une recherche de type "récupération" :
1. Saisir les valeurs suivantes :
   * Date de décès : 10/02/2020
   * Lieu de décès : VILLEFRANCHE
   * Date de l'acte de décès : 12/02/2020
   * Prénom : Aimé
   * Nom d'usage : OLMO
   * Date de naissance : 06/12/1977
1. Se rendre dans Liste des recherches
1. Sélectionner les filtres suivants :
    * Type de réponse "Récupération"
    
Résultat attendu :
    * La liste de toutes les recherches "Récupération" s'affichent
    * La recherche effectuée précédement s'affiche en haut du tableau
