#!/bin/bash

/usr/bin/mysqld_safe > /dev/null 2>&1 &

RET=1
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MySQL service startup"
    sleep 5
    mysql -uroot -e "status" > /dev/null 2>&1
    RET=$?
done

mysql -uroot -e "CREATE USER 'notaires'@'localhost' IDENTIFIED BY 'password'"
mysql -uroot -e "GRANT USAGE ON *.* TO  'notaires'@'%' IDENTIFIED BY 'password'"
mysql -uroot -e "CREATE DATABASE IF NOT EXISTS notaires"
mysql -uroot -e "GRANT ALL PRIVILEGES ON notaires.* TO 'notaires'@'%'"

echo "=> Done!"

mysqladmin -uroot shutdown
