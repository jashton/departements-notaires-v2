#!/bin/bash

bin/console cache:clear

bin/console assets:install --symlink

bin/console cache:warmup
