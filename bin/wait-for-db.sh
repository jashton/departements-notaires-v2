#!/bin/sh

set -e

DB_USER=$1
DB_PASSWORD=$2
i=5

until docker-compose exec database mysql -h database -u "${DB_USER}" --password="${DB_PASSWORD}" -e '\q' >/dev/null; do
    if [ $i -ge 60 ]; then
        echo "Database is unavailable (${i}s) - exit"
        exit 1
    fi

    echo "Database is unavailable (${i}s) - sleeping"
    sleep 5
    i=$((i + 5))
done

echo "Database is up"
