#!/bin/bash

# Base env
PROJECT='adullact-departements_et_notaires'
ZULIP_STREAM='cms-symfony'
NEXUS_REPO='https://nexus3-ovh.priv.atolcd.com/repository/adullact-departements_et_notaires'
PHP_VERSION=$(grep "PHP_VERSION" .env.dist | cut -d'=' -f2)
COMPOSER_VERSION=$(grep "COMPOSER_VERSION" .env.dist | cut -d'=' -f2)
NODE_VERSION=$(grep "NODE_VERSION" .env.dist | cut -d'=' -f2)

COMPOSER_ASK_VERSION=$(jq -r ".version" appli_sf/composer.json)
GIT_DESCRIBE_VERSION=$(git describe --long)
VERSION_EXISTS=$(git rev-parse "${COMPOSER_ASK_VERSION}" >/dev/null 2>&1 && echo "1" || echo "0")
VERSION_IS_FOR_TESTING=$([[ $COMPOSER_ASK_VERSION == 9999* ]] && echo "1" || echo "0")

if [[ "${VERSION_IS_FOR_TESTING}" -eq 1 ]]; then
    TYPE="snapshot"
    VERSION=$COMPOSER_ASK_VERSION
elif [ "${VERSION_EXISTS}" -eq 0 ]; then
    TYPE="release"
    VERSION=$COMPOSER_ASK_VERSION
else
    TYPE="snapshot"
    VERSION=$GIT_DESCRIBE_VERSION
fi

# Compose env
BASE_URL_UPLOAD="${NEXUS_REPO}-${TYPE}"
APP_FILENAME_TAR="app_${VERSION}.tgz"
LATEST_URL="${NEXUS_REPO}-latest"
LATEST_APP_FILENAME="app_latest-tar.txt"

case "$1" in
    PROJECT)                 echo "${PROJECT}"; exit 0;;
    ZULIP_STREAM)            echo "${ZULIP_STREAM}"; exit 0;;
    NEXUS_REPO)              echo "${NEXUS_REPO}"; exit 0;;
    TYPE)                    echo "${TYPE}"; exit 0;;
    VERSION)                 echo "${VERSION}"; exit 0;;
    VERSION_IS_FOR_TESTING)  echo "${VERSION_IS_FOR_TESTING}"; exit 0;;
    BASE_URL_UPLOAD)         echo "${BASE_URL_UPLOAD}"; exit 0;;
    APP_FILENAME_TAR)        echo "${APP_FILENAME_TAR}"; exit 0;;
    LATEST_URL)              echo "${LATEST_URL}"; exit 0;;
    LATEST_APP_FILENAME)     echo "${LATEST_APP_FILENAME}"; exit 0;;
    PHP_VERSION)             echo "${PHP_VERSION}"; exit 0;;
    COMPOSER_VERSION)        echo "${COMPOSER_VERSION}"; exit 0;;
    NODE_VERSION)            echo "${NODE_VERSION}"; exit 0;;
esac

exit 1
