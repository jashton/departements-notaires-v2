BIN_DIR			?= ./bin/

DOCKER_COMPOSE	= $(BIN_DIR)docker-compose
EXEC_PHP		= $(BIN_DIR)php

CONSOLE			= $(BIN_DIR)console
COMPOSER		= $(BIN_DIR)composer
YARN 			= $(BIN_DIR)yarn
WAIT_FOR_DB		= $(BIN_DIR)wait-for-db.sh

include .env

# Env handle
.env: .env.dist
	@if [ -f .env ]; \
	then\
		echo '/!\ The .env.dist file has changed. Please check your .env file (this message will not be displayed again).';\
		touch .env;\
		exit 1;\
	else\
		echo cp .env.dist .env;\
		cp .env.dist .env;\
	fi


##
## Docker
## ------
##

docker-build: ## Build stack from Dockerfile or Pull image from hub
docker-build: .env
	$(DOCKER_COMPOSE) pull --ignore-pull-failures
	$(DOCKER_COMPOSE) build --pull

docker-kill: ## Kill stack, remove container & volume (you loose your database)
docker-kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

docker-start: ## Start all docker based on docker-compose.json
docker-start: .env
	@$(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate

docker-stop: ## Gracefully shutdown the stack
docker-stop:
	@$(DOCKER_COMPOSE) stop

docker-down: ## Shutdown the stack and remove datas
docker-down:
	@$(DOCKER_COMPOSE) down -v

docker-restart: ## Gracefully restart stack
docker-restart: docker-stop docker-start

docker-refresh: ## Gracefully restart stack
docker-refresh: docker-stop docker-rm-no-data docker-start

docker-rm-no-data: ## Remove docker that doesn't own datas
docker-rm-no-data:
	@$(DOCKER_COMPOSE) rm -f php
	@$(DOCKER_COMPOSE) rm -f http
	@$(DOCKER_COMPOSE) rm -f mail

docker-force-restart: ## Kill container and restart
docker-force-restart: docker-kill install

docker-recreate: ## Down & Start
docker-recreate: docker-down docker-start

docker-rebuild: ## Kill, Build & Start
docker-rebuild: docker-kill install

docker-status: ## Print status
docker-status:
	@$(DOCKER_COMPOSE) ps

docker-php-status: ## Test if PHP container run
docker-php-status:
	@if [[ `$(DOCKER_COMPOSE) ps php | grep "Up" | wc -l` -ne 1 ]]; \
	 then \
		exit 1; \
	fi; > /dev/null 2>&1

.PHONY: docker-kill docker-stop docker-restart docker-force-restart docker-recreate docker-status docker-php-status docker-down docker-rebuild


##
## Composer
## --------
##

# rules based on files
composer.lock: ## Install all dependency using composer
composer.lock: ./appli_sf/composer.json
	$(COMPOSER) update --lock --no-scripts --no-interaction

composer-update: ## Update composer.lock and dependency based on constraint
composer-update:
	$(COMPOSER) update

composer-install: ## Install all dependency using composer
composer-install: ./appli_sf/composer.lock
	$(COMPOSER) install


##
## Project
## -------
##

install: ## Install and start the project
install: .env droits docker-build docker-start wait-for-db composer-install assets

update: ## Update the project
update: composer-update yarn.lock

droits: ## Add execution on bin files
droits:
	chmod +x bin/*
	chmod +x appli_sf/bin/*
	find . -type f -name "*.sh" -exec chmod 755 {} \;

assets: ## Run Webpack Encore to compile assets (dev)
assets: node_modules
	$(YARN) run dev

assets-build: ## Run Webpack Encore to compile assets (prod)
assets-build: node_modules
	$(YARN) run build

watch: ## Run Webpack Encore in watch mode
watch: node_modules
	$(YARN) run watch

.PHONY: install update droits assets assets-build watch


node_modules: ./appli_sf/yarn.lock
	$(YARN) install
	@touch -c node_modules

yarn.lock: ./appli_sf/package.json
	$(YARN) upgrade


wait-for-db:
	echo ${DB_USER}
	$(WAIT_FOR_DB) ${DB_USER} ${DB_PASSWORD}



# Default goal and help

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
