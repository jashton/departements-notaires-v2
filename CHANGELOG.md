# Changelog

All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

- Migration V1 vers V2
- Alerte décès
- Documentation utilisateur

## 2.0.0-beta.4 - 2020-07-16

### Added

Pour les utilisateurs :
- [#11 Exemple de CSS personnalisée](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/11)
- [#66 Détail des cheminements des recherches](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/66)

Pour les développeurs :
- [#53 Ajout de tests E2E](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/53)
- [#61 Ajout de tests unitaires dans CI Gitlab](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/61)
- [#69 Ajout de tests E2E sur les intructeurs](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/69)

### Fixed

- [#55 Suppression d'un message d'erreur lors d'une recherche lorsque la personne est connue](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/55)
- [#63 Correction de l'impossibilité de réinitialiser le mot de passe](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/63)
- [#64 Modification du titre de l'email de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/64)

## 2.0.0-beta.3 - 2020-07-01

### Added

- Template (capacité à modifier l'apparence de l'application)

### Fixed

Pour les utilisateurs :

- [#41 DOC algorithme de recherche: documentation complète](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/41)
- [#58 DOC Imprécision dans la documentation de l'algorithme de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/58)
- [#59 Amélioration le phrasé du mail quand le cas est connu](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/59)
- [#57 Informations sur la personne recherchée différentes dans le mail et le PDF](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/57)
- [#50 Dans le mail et le courrier PDF adressés aux notaires ne figure pas le nom d'état civil](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/50)
- [#49 Création d'une étude notariale : le complément d'adresse devrait être optionnel](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/49)
- [#47 Utilisation de liens symboliques sur des dossiers dans l'application](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/47)
- [#46 Rendre possible la recherche sur le nom d'usage (et non celui d'état civil)](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/46)
- [#45 Erreur à la création d'utilisateur au profil notaire](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/45)
- [#43 Lien en dur vers le Rhône sur la page d'accueil](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/43)
- [#40 Le prénom3 n'apparait pas dans les mails envoyés au notaire](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/40)
- [#19 Impossible de rafraichir les stats apres saisies des dates de selection](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/19)

Pour les développeurs :

- [#60 TEST avoir un jeu de test pour les noms d'état civil](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/60)
- [#38 Ajout de tests unitaires sur la recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/38)

## 2.0.0-beta.2 - 2020-06-12

### Added

- Génération et envoi automatique des PDFs associés aux mails après une recherche
- Extraction PDFs des listes des recherches et des statistiques
- Import des données (individus)
- Possibilité de personnaliser les courriers électroniques et PDF 
- Ajout du schéma de la base de données

### Changed

- Changelog now based on [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/)
- Ajout de prénom3 dans la recherche
- Ajout d'un paramètre pour choisir si la recherche doit porter sur le nom d'usage ou le nom d'état civil
- Mise à jour des libellés des paramètres
- Ajout du nom d'état civil dans les logs de recherche

### Fixed

- [#39](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/39) Le 3° prénom ne semble pas pris en compte
- [#37](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/37) Les données de tests ne produisent pas les résultats attendus
- [#35](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/35) L'ajout d'un logo de département mène à une erreur 500
- [#34](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/34) Erreur 500 sur ajout d'un utilisateur
- [#31](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/31) Une recherche sur base vide se termine en erreur 500
- [#30](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/30) L'export PDF des stats génère une erreur 500
- [#29](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/29) Import CSV: une ligne vide génère une erreur
- [#20](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/20) Envoi du mail à un
 instructeur (cas ambigu)
- [#14](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/14) Initiales vides pour un
 instructeur
- [#6](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/6) Affichage du logo SVG
- [#1](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/1), 
  [#16](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/16) Accessibilité de la
 page d'accueil

## 2.0.0-beta.1 - 2020-04-30

### Added

- Réglages des paramètres de l'application
- Consultation de l'historique des accès
- Gestion des comptes utilisateurs
- Gestion des instructeurs (pour le routage des emails)
- Recherche
- Liste des recherches
- Statistiques
- Recherche soundex (activable ou non dans les paramètres)
- Responsive design
