<?php

declare(strict_types=1);

namespace App\Imagine\Filter;

use Liip\ImagineBundle\Binary\BinaryInterface;
use Liip\ImagineBundle\Imagine\Filter\FilterManager as BaseFilterManager;
use Liip\ImagineBundle\Model\Binary;

class FilterManager extends BaseFilterManager
{
    /**
     * {@inheritdoc}
     */
    public function apply(BinaryInterface $binary, array $config): BinaryInterface
    {
        if ('svg' === $binary->getFormat() && !isset($config['format'])) {
            $config['format'] = 'png';
        }

        return parent::apply($binary, $config);
    }
}
