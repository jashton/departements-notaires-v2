<?php

declare(strict_types=1);

namespace App\Imagine\Cache\Resolver;

use Liip\ImagineBundle\Imagine\Cache\Resolver\WebPathResolver as BaseWebPathResolver;

class WebPathResolver extends BaseWebPathResolver
{
    /**
     * {@inheritdoc}
     */
    protected function getFileUrl($path, $filter): string
    {
        $path = preg_replace('/\.svg$/', '.png', $path);

        return parent::getFileUrl($path, $filter);
    }
}
