<?php

namespace App\DataFixtures;

use App\Helper\FixturesHelper;
use App\Helper\SearchHelper;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

class PersonUseNameFixtures extends AbstractPersonFixtures implements FixtureGroupInterface
{
    protected $nameToUse = SearchHelper::SEARCH_BY_USE_NAME;

    public static function getGroups(): array
    {
        return [FixturesHelper::GROUP_USE_NAME];
    }
}
