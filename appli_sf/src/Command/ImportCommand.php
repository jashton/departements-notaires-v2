<?php
// Pour lancer l'import client
// déposer le fichier import.csv dans imports (fichier d'exemple individus.csv)
// bin/console import:individus "imports/individus.csv"

namespace App\Command;

use App\Entity\Person;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportCommand extends Command
{
    private $doctrine;

    /**
     * @var SymfonyStyle
     */
    private $io;

    public function __construct(ManagerRegistry $doctrine)
    {
        parent::__construct();
        $this->doctrine = $doctrine;
    }

    protected function configure()
    {
        $this
            ->setName('import:individus')
            ->setDescription('Importer un fichier csv individus')
            ->setHelp('Permet d\'importer un fichier csv individus avec le chemin du fichier en paramètre.')
            ->addArgument('file', InputArgument::REQUIRED, 'The file path to import')
            ->addOption(
                'use-civil-name',
                null,
                InputOption::VALUE_OPTIONAL,
                'Utiliser le nom civil comme nom principal',
                0
            );
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $noInteraction = $input->getOption('no-interaction');
        $useCivilName = boolval($input->getOption('use-civil-name'));
        $output->setFormatter(new OutputFormatter(true));
        $hasHeaders = false;

        $this->io->section('Préparation de l\'import des individus');
        $path = realpath(__DIR__.'/../../').'/'.$input->getArgument('file');
        $this->io->writeln('Fichier à importer : '.$path);

        if (!is_file($path)) {
            $this->io->newLine();
            $this->io->error('Le fichier '.$path.' n\'a pas été trouvé.');

            return Command::FAILURE;
        }

        $contentFile = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $nbAImport = count($contentFile);

        if (!is_array($contentFile) || count($contentFile) == 0) {
            $this->io->warning('Aucun individu à importer !');

            return Command::FAILURE;
        }

        if (strpos($contentFile[0], 'num_ind') !== false) {
            $nbAImport--;
            $hasHeaders = true;
        }

        $this->io->writeln($nbAImport.' individus à importer');

        $this->io->caution(
            "Tous les individus actuellement dans la base données vont être effacés !"
        );
        $continue = $this->io->confirm(
            "Voulez-vous continuez ?",
            false
        );

        if ($continue || $noInteraction) {
            $this->deleteAllIndividus();
            $this->io->success('Suppression des individus existants');
            $this->io->newLine();

            $table = new Table($output);

            $this->io->section('Import des individus');
            $this->io->progressStart($nbAImport);

            foreach ($contentFile as $lineNb => $contentFileLine) {
                if ($lineNb === 0 && $hasHeaders) {
                    continue;
                }

                $array_value = [];
                $contentFileLineDetail = str_getcsv($contentFileLine, ';');

                if ($contentFileLineDetail[0] != 'num_ind') {
                    $array_value['num_ind'] = (int)trim($contentFileLineDetail[0]);
                    $array_value['sexe'] = trim($contentFileLineDetail[1]);
                    $array_value['nom_usage'] = $useCivilName ?
                        trim($contentFileLineDetail[3]) : trim($contentFileLineDetail[2]);
                    $array_value['nom_civil'] = $useCivilName ?
                        trim($contentFileLineDetail[2]) : trim($contentFileLineDetail[3]);
                    $array_value['prenom'] = trim($contentFileLineDetail[4]);
                    $array_value['prenomd'] = trim($contentFileLineDetail[5]);
                    $array_value['prenomt'] = trim($contentFileLineDetail[6]);
                    $array_value['date_naissance'] = trim($contentFileLineDetail[7]);
                    $array_value['adresse'] = trim($contentFileLineDetail[8]);
                    $array_value['mdr'] = trim($contentFileLineDetail[9]);
                    $array_value['telephone'] = trim($contentFileLineDetail[10]);
                    $array_value['mail_mdr'] = trim($contentFileLineDetail[11]);
                    $array_value['libelle'] = trim($contentFileLineDetail[12]);
                    $array_value['code'] = trim($contentFileLineDetail[13]);

                    try {
                        $this->addIndividu($array_value);
                    } catch (\Exception $e) {
                        $retour = $e->getMessage();
                        $table->addRow(
                            ['Individu <error>#'.$array_value['num_ind'].'</error>', '<error>'.$retour.'</error>']
                        );
                    }
                }
                $this->io->progressAdvance();
            }

            $this->io->progressFinish();
            $this->io->newLine();
            $table->render();
            $this->io->success('Fin de l\'import');
        }

        return Command::SUCCESS;
    }

    private function deleteAllIndividus()
    {
        $entityManager = $this->doctrine->getManager();
        $repository = $entityManager->getRepository(Person::class);
        $entities = $repository->findAll();

        foreach ($entities as $entity) {
            $entityManager->remove($entity);
        }
        $entityManager->flush();

        return true;
    }

    private function convertDate($date)
    {
        if (strpos($date, '/') === false) {
            return $date;
        } else {
            $date_p = explode('/', $date);

            return $date_p[2].'-'.$date_p[1].'-'.$date_p[0];
        }
    }

    private function addIndividu($array_value)
    {
        $entityManager = $this->doctrine->getManager();
        $individu = new Person();

        $cr_date = new \DateTime($this->convertDate($array_value['date_naissance']));

        $individu
            ->setPersonNum($array_value['num_ind'])
            ->setGender($array_value['sexe'])
            ->setCivilName($array_value['nom_civil'])
            ->setUseName($array_value['nom_usage'])
            ->setFirstName($array_value['prenom'])
            ->setMiddleName($array_value['prenomd'])
            ->setThirdName($array_value['prenomt'])
            ->setBirthDate($cr_date)
            ->setAddress($array_value['adresse'])
            ->setDepartmentalHouseName($array_value['mdr'])
            ->setDepartmentalHouseMail($array_value['mail_mdr'])
            ->setPhone($array_value['telephone'])
            ->setLabel($array_value['libelle'])
            ->setHelpCode($array_value['code']);

        $entityManager->persist($individu);
        $entityManager->flush();

        return $individu->getId();
    }
}
