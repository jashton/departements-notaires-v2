<?php

namespace App\Command;

use App\Entity\Client;
use App\Entity\User;
use App\Helper\UserHelper;
use App\Repository\UserRepository;
use App\Utility\Validator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Contracts\Translation\TranslatorInterface;
use function Symfony\Component\String\u;

class AddClientCommand extends Command
{
    protected static $defaultName = 'user:add';

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $entityManager;
    private $passwordEncoder;
    private $translator;
    private $userRepository;
    private $validator;

    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        Validator $validator
    ) {
        parent::__construct();

        $this->entityManager = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
        $this->userRepository = $userRepository;
        $this->validator = $validator;
    }

    protected function configure()
    {
        $this
            ->setDescription('Ajout d\'un nouvel utilisateur en base de données')
            ->addOption('username', 'u', InputOption::VALUE_REQUIRED, 'Identifiant du nouvel utilisateur')
            ->addOption('password', 'p', InputOption::VALUE_REQUIRED, 'Mot de passe du nouvel utilisateur')
            ->addOption('email', 'm', InputOption::VALUE_REQUIRED, 'Email du nouvel utilisateur')
            ->addOption('role', 'r', InputOption::VALUE_REQUIRED, 'Role du nouvel utilisateur')
            ->addOption('name', 'd', InputOption::VALUE_REQUIRED, 'Nom du nouvel utilisateur')
            ->addOption('service', null, InputOption::VALUE_OPTIONAL, 'Service du nouvel utilisateur')
            ->addOption('address', null, InputOption::VALUE_OPTIONAL, 'Adresse du nouvel utilisateur')
            ->addOption(
                'additional-address',
                null,
                InputOption::VALUE_OPTIONAL,
                'Complément d\'adresse du nouvel utilisateur'
            )
            ->addOption('postal-code', null, InputOption::VALUE_OPTIONAL, 'Code postal du nouvel utilisateur')
            ->addOption('town', null, InputOption::VALUE_OPTIONAL, 'Ville du nouvel utilisateur');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title("Ajout d'un utilisateur");

        $username = $input->getOption('username');

        if (null !== $username) {
            $this->io->text(' > <info>'.$this->translator->trans('addUserCmd.login').'</info>: '.$username);
        } else {
            $username = $this->io->ask(
                $this->translator->trans('addUserCmd.login'),
                null,
                [$this->validator, 'validateUsername']
            );
            $input->setOption('username', $username);
        }

        $email = $input->getOption('email');

        if (null !== $email) {
            $this->io->text(' > <info>'.$this->translator->trans('addUserCmd.email').'</info>: '.$email);
        } else {
            $email = $this->io->ask(
                $this->translator->trans('addUserCmd.email'),
                null,
                [$this->validator, 'validateEmail']
            );
            $input->setOption('email', $email);
        }

        $password = $input->getOption('password');

        if (null !== $password) {
            $this->io->text(
                ' > <info>'.$this->translator->trans('addUserCmd.password').'</info>: '
                .u('*')->repeat(u($password)->length())
            );
        } else {
            $password = $this->io->askHidden(
                $this->translator->trans('addUserCmd.password'),
                [$this->validator, 'validatePassword']
            );

            $confirmPassword = $this->io->askHidden(
                $this->translator->trans('addUserCmd.confirmPassword'),
                function ($confirmPassword) use ($password) {
                    if (empty($confirmPassword)) {
                        throw new \RuntimeException($this->translator->trans('addUser.error.notEmpty'));
                    }

                    if ($confirmPassword !== $password) {
                        throw new \RuntimeException(
                            $this->translator->trans('addUser.error.confirmPasswordNoMatch')
                        );
                    }

                    return $confirmPassword;
                }
            );

            if ($password !== $confirmPassword) {
                $this->io->error($this->translator->trans('addUser.error.confirmPasswordNoMatch'));

                return 3;
            }

            $input->setOption('password', $password);
        }

        $role = $input->getOption('role');

        if (null !== $role) {
            $this->io->text(
                ' > <info>'.$this->translator->trans('addUserCmd.role').'</info>: '.$role
            );
        } else {
            $rolesList = [
                1 => $this->translator->trans('ROLE_NOTARY'),
                2 => $this->translator->trans('ROLE_AGENT'),
                3 => $this->translator->trans('ROLE_ADMIN'),
            ];
            $roleValue = $this->io->choice($this->translator->trans('addUserCmd.role'), $rolesList, 3);
            $roleKey = array_search($roleValue, $rolesList);
            $role = UserHelper::getRoleFromInt($roleKey);

            $input->setOption('role', $role);
        }

        $name = $input->getOption('name');

        if (null !== $name) {
            $this->io->text(
                ' > <info>'.$this->translator->trans('addUserCmd.name').'</info>: '.$name
            );
        } else {
            $name = $this->io->ask(
                $this->translator->trans('addUserCmd.name'),
                null,
                [$this->validator, 'validateNotEmpty']
            );

            $input->setOption('name', $name);
        }

        if ($role === UserHelper::ROLE_AGENT) {
            $service = $input->getOption('service');

            if (null !== $service) {
                $this->io->text(
                    ' > <info>'.$this->translator->trans('addUserCmd.service').'</info>: '.$service
                );
            } else {
                $service = $this->io->ask(
                    $this->translator->trans('addUserCmd.service'),
                    null,
                    [$this->validator, 'validateNotEmpty']
                );

                $input->setOption('service', $service);
            }
        }

        if ($role === UserHelper::ROLE_NOTARY) {
            $address = $input->getOption('address');

            if (null !== $address) {
                $this->io->text(
                    ' > <info>'.$this->translator->trans('addUserCmd.address').'</info>: '.$address
                );
            } else {
                $address = $this->io->ask(
                    $this->translator->trans('addUserCmd.address'),
                    null,
                    [$this->validator, 'validateNotEmpty']
                );

                $input->setOption('address', $address);
            }


            $additionalAddress = $input->getOption('additional-address');

            if (null !== $additionalAddress) {
                $this->io->text(
                    ' > <info>'.$this->translator->trans('addUserCmd.additionalAddress').'</info>: '.$additionalAddress
                );
            } else {
                $additionalAddress = $this->io->ask(
                    $this->translator->trans('addUserCmd.additionalAddress'),
                    null,
                    [$this->validator, 'validateNotEmpty']
                );

                $input->setOption('additional-address', $additionalAddress);
            }


            $postalCode = $input->getOption('postal-code');

            if (null !== $postalCode) {
                $this->io->text(
                    ' > <info>'.$this->translator->trans('addUserCmd.postalCode').'</info>: '.$postalCode
                );
            } else {
                $postalCode = $this->io->ask(
                    $this->translator->trans('addUserCmd.postalCode'),
                    null,
                    [$this->validator, 'validateNotEmpty']
                );

                $input->setOption('postal-code', $postalCode);
            }


            $town = $input->getOption('town');

            if (null !== $town) {
                $this->io->text(
                    ' > <info>'.$this->translator->trans('addUserCmd.town').'</info>: '.$town
                );
            } else {
                $town = $this->io->ask(
                    $this->translator->trans('addUserCmd.town'),
                    null,
                    [$this->validator, 'validateNotEmpty']
                );

                $input->setOption('town', $town);
            }
        }
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {

        $stopwatch = new Stopwatch();
        $stopwatch->start('add-user-command');

        $username = $input->getOption('username');
        $plainPassword = $input->getOption('password');
        $email = $input->getOption('email');
        $role = $input->getOption('role');
        $name = $input->getOption('name');
        $service = $input->getOption('service');
        $address = $input->getOption('address');
        $additionalAddress = $input->getOption('additional-address');
        $postalCode = $input->getOption('postal-code');
        $town = $input->getOption('town');

        $this->validateUserData(
            $username,
            $plainPassword,
            $email,
            $name,
            $role,
            $service,
            $address,
            $additionalAddress,
            $postalCode,
            $town
        );

        $user = new User();
        $user->setUsername($username);

        $encodedPassword = $this->passwordEncoder->encodePassword($user, $plainPassword);
        $user->setPassword($encodedPassword);

        $user->setEmail($email);
        $user->setRoles([$role]);
        $user->setName($name);
        $user->setService($service);
        $user->setAddress($address);
        $user->setAdditionalAddress($additionalAddress);
        $user->setPostalCode($postalCode);
        $user->setTown($town);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->io->success($this->translator->trans('addUser.success.userCreated', [$username, $role]));

        $event = $stopwatch->stop('add-user-command');
        if ($output->isVerbose()) {
            $this->io->comment(
                sprintf(
                    'New user database id: %d / Elapsed time: %.2f ms / Consumed memory: %.2f MB',
                    $user->getId(),
                    $event->getDuration(),
                    $event->getMemory() / (1024 ** 2)
                )
            );
        }

        return Command::SUCCESS;
    }

    /**
     * @param string $username
     * @param string $plainPassword
     * @param string $email
     * @param string $name
     * @param string $role
     * @param string $service
     * @param string $address
     * @param string $additionalAddress
     * @param string $postalCode
     * @param string $town
     */
    private function validateUserData(
        $username,
        $plainPassword,
        $email,
        $name,
        $role,
        $service = '',
        $address = '',
        $additionalAddress = '',
        $postalCode = '',
        $town = ''
    ): void {
        // first check if a user with the same username already exists.
        $existingUser = $this->userRepository->findOneBy(['username' => $username]);

        if (null !== $existingUser) {
            throw new \RuntimeException(
                sprintf('There is already a user registered with the "%s" username.', $username)
            );
        }

        // validate password and email if is not this input means interactive.
        $this->validator->validatePassword($plainPassword);
        $this->validator->validateEmail($email);
        $this->validator->validateNotEmpty($name, 'name');

        if ($role === UserHelper::ROLE_AGENT) {
            $this->validator->validateNotEmpty($service, 'service');
        }

        if ($role === UserHelper::ROLE_NOTARY) {
            $this->validator->validateNotEmpty($address, 'address');
            $this->validator->validateNotEmpty($additionalAddress, 'additionalAddress');
            $this->validator->validateNotEmpty($postalCode, 'postalCode');
            $this->validator->validateNotEmpty($town, 'town');
        }
    }
}
