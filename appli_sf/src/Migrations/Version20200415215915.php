<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200415215915 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE log_membre DROP FOREIGN KEY FK_E34EA88BD0834EC4');
        $this->addSql('ALTER TABLE log_membre CHANGE id_membre id_membre INT DEFAULT NULL');
        $this->addSql('ALTER TABLE log_membre ADD CONSTRAINT FK_E34EA88BD0834EC4 FOREIGN KEY (id_membre) REFERENCES membre (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE log_recherche DROP FOREIGN KEY FK_A22587C8D0834EC4');
        $this->addSql('ALTER TABLE log_recherche DROP FOREIGN KEY FK_A22587C8E3FC35B');
        $this->addSql('ALTER TABLE log_recherche CHANGE id_membre id_membre INT DEFAULT NULL');
        $this->addSql('ALTER TABLE log_recherche ADD CONSTRAINT FK_A22587C8D0834EC4 FOREIGN KEY (id_membre) REFERENCES membre (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE log_recherche ADD CONSTRAINT FK_A22587C8E3FC35B FOREIGN KEY (id_individu) REFERENCES individu (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE log_membre DROP FOREIGN KEY FK_E34EA88BD0834EC4');
        $this->addSql('ALTER TABLE log_membre CHANGE id_membre id_membre INT NOT NULL');
        $this->addSql('ALTER TABLE log_membre ADD CONSTRAINT FK_E34EA88BD0834EC4 FOREIGN KEY (id_membre) REFERENCES membre (id)');
        $this->addSql('ALTER TABLE log_recherche DROP FOREIGN KEY FK_A22587C8D0834EC4');
        $this->addSql('ALTER TABLE log_recherche DROP FOREIGN KEY FK_A22587C8E3FC35B');
        $this->addSql('ALTER TABLE log_recherche CHANGE id_membre id_membre INT NOT NULL');
        $this->addSql('ALTER TABLE log_recherche ADD CONSTRAINT FK_A22587C8D0834EC4 FOREIGN KEY (id_membre) REFERENCES membre (id)');
        $this->addSql('ALTER TABLE log_recherche ADD CONSTRAINT FK_A22587C8E3FC35B FOREIGN KEY (id_individu) REFERENCES individu (id)');
    }
}
