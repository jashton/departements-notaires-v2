<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200331191043 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE individu (id INT AUTO_INCREMENT NOT NULL, sexe VARCHAR(1) NOT NULL, nom_usage VARCHAR(255) NOT NULL, nom_civil VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) NOT NULL, prenomd VARCHAR(255) DEFAULT NULL, prenomt VARCHAR(255) DEFAULT NULL, date_naissance DATE NOT NULL, adresse VARCHAR(255) DEFAULT NULL, mdr VARCHAR(255) DEFAULT NULL, telephone VARCHAR(255) DEFAULT NULL, mail_mdr VARCHAR(255) DEFAULT NULL, libelle VARCHAR(255) DEFAULT NULL, code_aide VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE individu');
    }
}
