<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 * @ORM\Table(name="individu", indexes={@ORM\Index(name="num_ind", columns={"num_ind"})})
 */
class Person extends AbstractPerson
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="num_ind", type="integer", unique=true)
     */
    private $personNum;

    /**
     * @ORM\Column(name="sexe", type="string", length=1)
     */
    private $gender;

    /**
     * @ORM\Column(name="nom_usage", type="string", length=255, nullable=true)
     */
    private $useName;

    /**
     * @ORM\Column(name="nom_civil", type="string", length=255, nullable=true)
     */
    private $civilName;

    /**
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(name="prenomd", type="string", length=255, nullable=true)
     */
    private $middleName;

    /**
     * @ORM\Column(name="prenomt", type="string", length=255, nullable=true)
     */
    private $thirdName;

    /**
     * @ORM\Column(name="date_naissance", type="date")
     */
    private $birthDate;

    /**
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(name="mdr", type="string", length=255, nullable=true)
     */
    private $DepartmentalHouseName;

    /**
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(name="mail_mdr", type="string", length=255, nullable=true)
     */
    private $DepartmentalHouseMail;

    /**
     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(name="code_aide", type="string", length=50, nullable=true)
     */
    private $helpCode;

    /**
     * @ORM\Column(name="date_deces", type="date")
     */
    private $deathDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonNum(): ?int
    {
        return $this->personNum;
    }

    public function setPersonNum(int $personNum): self
    {
        $this->personNum = $personNum;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getUseName(): ?string
    {
        return $this->useName;
    }

    public function setUseName(?string $useName): self
    {
        $this->useName = $useName;

        return $this;
    }

    public function getCivilName(): ?string
    {
        return $this->civilName;
    }

    public function setCivilName(?string $civilName): self
    {
        $this->civilName = $civilName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function setMiddleName(?string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getThirdName(): ?string
    {
        return $this->thirdName;
    }

    public function setThirdName(?string $thirdName): self
    {
        $this->thirdName = $thirdName;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getDepartmentalHouseName(): ?string
    {
        return $this->DepartmentalHouseName;
    }

    public function setDepartmentalHouseName(?string $DepartmentalHouseName): self
    {
        $this->DepartmentalHouseName = $DepartmentalHouseName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getDepartmentalHouseMail(): ?string
    {
        return $this->DepartmentalHouseMail;
    }

    public function setDepartmentalHouseMail(?string $DepartmentalHouseMail): self
    {
        $this->DepartmentalHouseMail = $DepartmentalHouseMail;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getHelpCode(): ?string
    {
        return $this->helpCode;
    }

    public function setHelpCode(?string $helpCode): self
    {
        $this->helpCode = $helpCode;

        return $this;
    }

    public function getDeathDate(): ?\DateTimeInterface
    {
        return $this->deathDate;
    }

    public function setDeathDate(?\DateTimeInterface $deathDate): self
    {
        $this->deathDate = $deathDate;

        return $this;
    }
}
