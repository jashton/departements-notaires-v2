<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SearchLogRepository")
 * @ORM\Table(name="log_recherche")
 */
class SearchLog extends AbstractPerson
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="searchLogs")
     * @ORM\JoinColumn(name="id_membre", nullable=true, onDelete="SET NULL")
     */
    private $user;

    /**
     * @ORM\Column(name="nom_usage", type="string", length=255, nullable=true)
     */
    private $useName;

    /**
     * @ORM\Column(name="nom_civil", type="string", length=255, nullable=true)
     */
    private $civilName;

    /**
     * @var array
     *
     * @ORM\Column(name="prenom", type="json")
     */
    private $firstNames = [];

    /**
     * @ORM\Column(name="date_naissance", type="date")
     */
    private $birthDate;

    /**
     * @ORM\Column(name="date_deces", type="date")
     */
    private $deathDate;

    /**
     * @ORM\Column(name="lieu_deces", type="string", length=255)
     */
    private $deathLocation;

    /**
     * @ORM\Column(name="date_acte_deces", type="date")
     */
    private $deathCertificateDate;

    /**
     * @ORM\Column(name="type_reponse", type="integer")
     */
    private $responseType;

    /**
     * @ORM\Column(name="date_recherche", type="datetime")
     */
    private $searchDate;

    /**
     * @ORM\Column(name="id_individu", type="integer", nullable=true)
     */
    private $person;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUseName(): ?string
    {
        return $this->useName;
    }

    public function setUseName(?string $useName): self
    {
        $this->useName = $useName;

        return $this;
    }

    public function getCivilName(): ?string
    {
        return $this->civilName;
    }

    public function setCivilName(?string $civilName)
    {
        $this->civilName = $civilName;

        return $this;
    }


    public function getFirstNames(): array
    {
        return $this->firstNames;
    }

    public function setFirstNames(array $firstNames): self
    {
        $this->firstNames = $firstNames;

        return $this;
    }

    public function addFirstName(string $firstName): self
    {
        $this->firstNames[] = $firstName;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstNames[0];
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getDeathDate(): ?\DateTimeInterface
    {
        return $this->deathDate;
    }

    public function setDeathDate(\DateTimeInterface $deathDate): self
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    public function getDeathLocation(): ?string
    {
        return $this->deathLocation;
    }

    public function setDeathLocation(string $deathLocation): self
    {
        $this->deathLocation = $deathLocation;

        return $this;
    }

    public function getDeathCertificateDate(): ?\DateTimeInterface
    {
        return $this->deathCertificateDate;
    }

    public function setDeathCertificateDate(\DateTimeInterface $deathCertificateDate): self
    {
        $this->deathCertificateDate = $deathCertificateDate;

        return $this;
    }

    public function getResponseType(): ?int
    {
        return $this->responseType;
    }

    public function setResponseType(int $responseType): self
    {
        $this->responseType = $responseType;

        return $this;
    }

    public function getSearchDate(): ?\DateTimeInterface
    {
        return $this->searchDate;
    }

    public function setSearchDate(\DateTimeInterface $searchDate): self
    {
        $this->searchDate = $searchDate;

        return $this;
    }

    public function getPerson(): ?int
    {
        return $this->person;
    }

    public function setPerson(?int $person): self
    {
        $this->person = $person;

        return $this;
    }
}
