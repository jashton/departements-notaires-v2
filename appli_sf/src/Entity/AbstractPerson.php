<?php

namespace App\Entity;

use App\Data\SettingsData;
use App\Helper\SearchHelper;

abstract class AbstractPerson
{
    abstract protected function getCivilName();
    abstract protected function getUseName();

    public function getName(SettingsData $settingsData)
    {
        return $settingsData->getSearchByName() === SearchHelper::SEARCH_BY_CIVIL_NAME ?
            $this->getCivilName() : $this->getUseName();
    }
}
