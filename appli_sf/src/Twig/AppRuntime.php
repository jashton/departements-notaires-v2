<?php

namespace App\Twig;

use App\Helper\SearchHelper;
use App\Helper\UploaderHelper;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupInterface;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    private $publicDir;
    private $cacheManager;
    private $uploaderHelper;
    private $entrypointLookup;

    /**
     * AppExtension constructor.
     *
     * @param string                    $publicDir
     * @param CacheManager              $cacheManager
     * @param UploaderHelper            $uploaderHelper
     * @param EntrypointLookupInterface $entrypointLookup
     */
    public function __construct(
        string $publicDir,
        CacheManager $cacheManager,
        UploaderHelper $uploaderHelper,
        EntrypointLookupInterface $entrypointLookup
    ) {
        $this->publicDir = $publicDir;
        $this->cacheManager = $cacheManager;
        $this->uploaderHelper = $uploaderHelper;
        $this->entrypointLookup = $entrypointLookup;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File $file
     *
     * @return string
     */
    public function getUploadedAssetPath(?File $file): string
    {
        if ($file) {
            return $this->uploaderHelper
                ->getPublicPath($file);
        }

        return '';
    }

    /**
     * @param string $entryName
     *
     * @return string
     */
    public function getEncoreEntryCssSource(string $entryName): string
    {
        $files = $this->entrypointLookup
            ->getCssFiles($entryName);
        $source = '';
        foreach ($files as $file) {
            $source .= file_get_contents($this->publicDir.'/'.$file);
        }

        return $source;
    }

    /**
     * @param int $responseType
     *
     * @return bool
     */
    public function getSearchHasPdf(int $responseType): bool
    {
        return SearchHelper::hasLetterPdf($responseType);
    }

    /**
     * @param string $path
     * @param string $filter
     * @param array  $runtimeConfig
     * @param null   $resolver
     *
     * @return string
     */
    public function imageFilter(string $path, string $filter, array $runtimeConfig = [], $resolver = null)
    {
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        if ($ext === "svg") {
            return $path;
        } else {
            return $this->cacheManager->getBrowserPath($path, $filter, $runtimeConfig, $resolver);
        }
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function getFileContent(string $path): string
    {
        return file_get_contents($this->publicDir.'/'.$path);
    }
}
