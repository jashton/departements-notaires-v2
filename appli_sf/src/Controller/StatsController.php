<?php

namespace App\Controller;

use App\Entity\SearchLog;
use App\Entity\UserLog;
use App\Helper\GraphHelper;
use App\Helper\SearchHelper;
use App\Helper\StatsHelper;
use App\Repository\SearchLogRepository;
use App\Repository\UserLogRepository;
use App\Service\PdfService;
use App\Service\SettingsDataService;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class StatsController extends BaseController
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * UserController constructor.
     *
     * @param SettingsDataService $settingsDataService
     * @param TranslatorInterface $translator
     * @param string              $appTheme
     */
    public function __construct(
        SettingsDataService $settingsDataService,
        TranslatorInterface $translator,
        string $appTheme
    ) {
        parent::__construct($settingsDataService, $appTheme);

        $this->translator = $translator;
    }

    /**
     * Affichage du tableau des statistiques
     *
     * @Route("/stats", name="stats")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function statsAction(Request $request)
    {
        $dateTo = new DateTime();
        $dateTo->setTime(23, 59, 59, 999999);
        $dateFrom = clone $dateTo;
        $dateFrom
            ->modify('first day of -4 month') // affiche au maximum 5 mois
            ->setTime(0, 0); // force l'heure à 00:00

        [$stats, $tableHeader] = $this->getStats($dateFrom, $dateTo);

        $displayedStats = StatsHelper::verticalToHorizontalStatsArray($stats, $this->translator);

        // Création formumaire de téléchargement du pdf à partir d'une date de début et une date de fin
        $form = $this->createFormBuilder()
            ->add(
                'startDate',
                DateType::class,
                [
                    'label'    => 'searchLogsFilter.startDate',
                    'required' => true,
                    'widget'   => 'single_text',
                    'data'     => $dateFrom,
                ]
            )->add(
                'endDate',
                DateType::class,
                [
                    'label'    => 'searchLogsFilter.endDate',
                    'required' => true,
                    'widget'   => 'single_text',
                    'data'     => $dateTo,
                ]
            )->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'stats.sendPdf',
                ]
            )->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            /** @var DateTime $startDate */
            $startDate = $data['startDate'];
            /** @var DateTime $startDate */
            $endDate = $data['endDate'];

            return $this->redirectToRoute(
                'stats_pdf',
                [
                    'date_from' => $startDate->format('Y-m-d'),
                    'date_to'   => $endDate->format('Y-m-d'),
                ]
            );
        }

        return $this->render(
            'stats/stats.html.twig',
            [
                'tableHeader' => $tableHeader,
                'stats'       => $displayedStats,
                'dateFrom'    => $dateFrom,
                'dateTo'      => $dateTo,
                'statsForm'   => $form->createView(),
            ]
        );
    }

    /**
     * Affichage du graph
     *
     * @Route("/stats/graph", name="stats_graph")
     *
     * @param Request $request
     *
     * @throws \Exception
     */
    public function graphAction(Request $request)
    {
        $dateFrom = DateTime::createFromFormat('Y-m-d', $request->query->get('date_from'));
        $dateTo = DateTime::createFromFormat('Y-m-d', $request->query->get('date_to'));

        [$stats] = $this->getStats($dateFrom, $dateTo);

        $graph = new GraphHelper($this->translator);
        $graph->render($dateTo, $dateFrom, $stats, false);
    }

    /**
     * Affichage des stats dans un PDF
     *
     * @Route("/stats/pdf", name="stats_pdf")
     *
     * @return string
     * @throws \Exception
     */
    public function showPdfAction(Request $request, PdfService $pdfService)
    {
        $dateFrom = DateTime::createFromFormat('Y-m-d', $request->query->get('date_from'));
        $dateTo = DateTime::createFromFormat('Y-m-d', $request->query->get('date_to'));

        [$stats, $tableHeader] = $this->getStats($dateFrom, $dateTo);
        $displayedStats = StatsHelper::verticalToHorizontalStatsArray($stats, $this->translator);

        $graph = new GraphHelper($this->translator);
        $graphFile = $graph->render($dateTo, $dateFrom, $stats, true);

        $pdfContent = $this->renderView(
            'stats/pdf.html.twig',
            [
                'dateFrom'    => $dateFrom,
                'dateTo'      => $dateTo,
                'tableHeader' => $tableHeader,
                'stats'       => $displayedStats,
                'graphFile'   => $graphFile,
            ]
        );

        $fileName = 'notaires-statistiques_'.$dateFrom->format('Y-m').'_'.$dateTo->format('Y-m').'.pdf';
        $pdfService->create();
        $pdfService->generatePdf($pdfContent, $fileName, 'D');

        return $this->redirect('stats');
    }

    /**
     * Récupère les stats du site
     *
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     *
     * @return array
     * @throws \Exception
     */
    private function getStats(DateTime $dateFrom, DateTime $dateTo): array
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var SearchLogRepository $searchLogRepository */
        $searchLogRepository = $entityManager->getRepository(SearchLog::class);
        /** @var UserLogRepository $userLogRepository */
        $userLogRepository = $entityManager->getRepository(UserLog::class);

        $stats = [];
        $monthsList = [];
        $iDate = clone $dateTo;

        // Initialisation du tableau de stats
        while ($iDate >= $dateFrom) {
            $monthsList[] = $iDate->format('U');
            $stats[$iDate->format('Y-m')] = [
                'nbRequests'        => 0,
                'nbResponses'       => 0,
                'nbResponsesByType' => [
                    SearchHelper::SEARCH_STATUS_FIND_REC    => 0,
                    SearchHelper::SEARCH_STATUS_FIND_NOTREC => 0,
                    SearchHelper::SEARCH_STATUS_AMBIGUOUS   => 0,
                    SearchHelper::SEARCH_STATUS_NOTFIND     => 0,
                ],
                'nbConnections'     => 0,
                'nbUniqueUsers'     => 0,
            ];

            $iDate->modify('last day of previous month');
        }

        $searchStatsResult = $searchLogRepository->findSearchStats($dateFrom, $dateTo);

        // Remplissage du tableau de stats avec les données de recherche
        foreach ($searchStatsResult as $result) {
            if (!array_key_exists($result['month'], $stats)) {
                continue;
            }

            $curStats = &$stats[$result['month']];

            // Calcul du nombre total de réponses (filtre sur le mois + somme des valeurs de l'index 'nb')
            if ($curStats['nbResponses'] === 0) {
                $curMonthResults = array_filter(
                    $searchStatsResult,
                    function ($value) use ($result) {
                        return $value['month'] === $result['month'];
                    }
                );
                $curStats['nbResponses'] = array_sum(array_column($curMonthResults, 'nb'));
                $curStats['nbRequests'] = $curStats['nbResponses'];
            }

            $curStats['nbResponsesByType'][$result['responseType']] = $result['nb'];
        }

        // Nombre de connexions pour chaque mois
        $countLogResult = $userLogRepository->countLogByMonth($dateFrom, $dateTo);

        foreach ($countLogResult as $result) {
            if (!array_key_exists($result['month'], $stats)) {
                continue;
            }

            $curStats = &$stats[$result['month']];
            $curStats['nbConnections'] = $result['nb'];
        }

        // Nombre de connexions unique pour chaque mois
        $countUniqLogResult = $userLogRepository->countUniqLogByMonth($dateFrom, $dateTo);

        foreach ($countUniqLogResult as $result) {
            if (!array_key_exists($result['month'], $stats)) {
                continue;
            }

            $curStats = &$stats[$result['month']];
            $curStats['nbUniqueUsers'] = $result['nb'];
        }

        return [$stats, $monthsList];
    }
}
