<?php

namespace App\Controller;

use App\Data\SettingsData;
use App\Service\SettingsDataService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Twig\Error\LoaderError;

class BaseController extends AbstractController
{
    /**
     * @var SettingsData
     */
    protected $settingsData;

    /**
     * @var string
     */
    protected $appTheme;

    /**
     * BaseController constructor.
     *
     * @param SettingsDataService $settingsDataServiceService
     * @param string              $appTheme
     */
    public function __construct(SettingsDataService $settingsDataServiceService, string $appTheme = 'default')
    {
        $this->settingsData = $settingsDataServiceService->getData();
        $this->appTheme = $appTheme;
    }

    /**
     * Réécriture de la fonction renderView pour permettre la surchage des templates
     *
     * @param string $view
     * @param array  $parameters
     *
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function renderView(string $view, array $parameters = []): string
    {
        if (!$this->container->has('twig')) {
            throw new \LogicException(
                'You can not use the "renderView" method if the Twig Bundle is not available. Try running "composer require symfony/twig-bundle".'
            );
        }

        $themeView = $this->appTheme.'/'.$view;

        try {
            return $this->container->get('twig')->render($themeView, $parameters);
        } catch (LoaderError $e) {
            $view = 'default/'.$view;

            return $this->container->get('twig')->render($view, $parameters);
        }
    }
}
