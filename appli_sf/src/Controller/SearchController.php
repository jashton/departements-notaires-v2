<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Instructor;
use App\Entity\Person;
use App\Entity\SearchLog;
use App\Entity\User;
use App\Form\SearchLogsFiltersType;
use App\Form\SearchType;
use App\Helper\FileHelper;
use App\Helper\MessageHelper;
use App\Helper\SearchHelper;
use App\Helper\UserHelper;
use App\Repository\PersonRepository;
use App\Repository\SearchLogRepository;
use App\Service\MailService;
use App\Service\PdfService;
use App\Service\PersonService;
use App\Service\SettingsDataService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Exception\RfcComplianceException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class SearchController extends BaseController
{
    protected $translator;

    /**
     * UserController constructor.
     *
     * @param SettingsDataService $settingsDataService
     * @param TranslatorInterface $translator
     * @param string              $appTheme
     */
    public function __construct(
        SettingsDataService $settingsDataService,
        TranslatorInterface $translator,
        string $appTheme
    ) {
        parent::__construct($settingsDataService, $appTheme);

        $this->translator = $translator;
    }

    /**
     * Recherche d'un individu
     *
     * @Route("/search", name="search")
     *
     * @param Request     $request
     * @param MailService $mailService
     *
     * @return Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     */
    public function searchAction(
        Request $request,
        MailService $mailService,
        PdfService $pdfService,
        PersonService $personService
    ) {
        $searchData = new SearchData();
        $persons = null;

        $form = $this->createForm(SearchType::class, $searchData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            /** @var PersonRepository $personRepository */
            $personRepository = $entityManager->getRepository(Person::class);
            $useSoundex = $this->settingsData->hasSoundexSearch();
            $person = null;
            $searchStatus = SearchHelper::SEARCH_STATUS_NOTFIND;
            $sendMailToInstructor = true;

            [$searchStatus, $person] = $this->searchPerson($personRepository, $searchData, $useSoundex);

            // ----------------------
            // REPONSE_INCONNU : élargissement aux demande possible dans plusieurs cas, le traitement est mutualisé ici
            // ----------------------
            if ($searchStatus === SearchHelper::SEARCH_STATUS_NOTFIND) {
                $countPersons = $personRepository->countBySearch($searchData, $useSoundex, false, false, true);

                if ($countPersons === 1) {
                    $this->sendMail(SearchHelper::SEARCH_STATUS_HAS_REQUEST, $mailService, $searchData);
                    $sendMailToInstructor = false;
                }
            }

            $this->addFlash(
                MessageHelper::MESSAGE_INFO,
                $this->translator->trans('searchResult.responseType.'.$searchStatus)
            );

            // Envoie de l'alerte décès si la date de décès ne correspond pas à ce qui se trouve en base
            $this->sendDeathAlert($person, $searchData, $mailService);

            $searchLog = $this->addSearchLog($searchData, $searchStatus, $person);

            if ($sendMailToInstructor) {
                $validInstructors = $this->findInstructorsForPerson($searchLog, $searchStatus, $personService);

                $this->sendMail(
                    $searchStatus,
                    $mailService,
                    $searchData,
                    $validInstructors,
                    $searchLog,
                    $pdfService,
                    $personService
                );
            }

            return $this->redirectToRoute('search_result');
        }

        return $this->render(
            'search/search.html.twig',
            [
                'searchForm' => $form->createView(),
                'persons'    => $persons,
            ]
        );
    }

    /**
     * Affiche le résultat d'une recherche
     *
     * @Route("/search/result", name="search_result")
     *
     * @return Response
     */
    public function resultAction()
    {
        return $this->render('search/result.html.twig');
    }

    /**
     * Affiche les logs de recherche
     *
     * @Route("/search_logs", name="search-logs")
     *
     * @param Request  $request
     * @param Security $security
     *
     * @return Response
     */
    public function logsAction(Request $request, Security $security)
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(SearchLogsFiltersType::class);
        $form->handleRequest($request);
        $searchLogs = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            /** @var SearchLogRepository $searchLogRepository */
            $searchLogRepository = $entityManager->getRepository(SearchLog::class);

            $currentUser = null;

            if (!$security->isGranted(UserHelper::ROLE_AGENT)) {
                $currentUser = $user;
            }

            $searchLogs = $searchLogRepository->findByFilter($data, $currentUser);
        }

        return $this->render(
            'search/logs.html.twig',
            [
                'logsFiltersForm' => $form->createView(),
                'searchLogs'      => $searchLogs,
            ]
        );
    }

    /**
     * Affiche le PDF associé à une recherche
     *
     * @Route("/search_logs/{id}/letter", name="letter_pdf")
     *
     * @param SearchLog     $searchLog
     * @param PdfService    $pdfService
     * @param PersonService $personService
     *
     * @return Response|null
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function showLetterAction(
        SearchLog $searchLog,
        PdfService $pdfService,
        PersonService $personService
    ) {
        if ($this->isGranted(UserHelper::ROLE_AGENT) || $searchLog->getUser() === $this->getUser()) {
            $this->getPdf($searchLog, 'I', $pdfService, $personService);

            return null;
        } else {
            return $this->render(
                'utils/not_found.html.twig'
            );
        }
    }

    /**
     * Génère le PDF envoyé après une recherche
     *
     * @param SearchLog     $searchLog
     * @param string        $dest
     * @param PdfService    $pdfService
     * @param PersonService $personService
     *
     * @return array
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function getPdf(
        SearchLog $searchLog,
        string $dest,
        PdfService $pdfService,
        PersonService $personService
    ): array {
        /** @var User $user */
        $user = $this->getUser();
        $pdfTemplate = null;

        switch ($searchLog->getResponseType()) {
            case SearchHelper::SEARCH_STATUS_FIND_REC:
                $pdfTemplate = 'pdf/search_find_rec.html.twig';

                break;
            case SearchHelper::SEARCH_STATUS_FIND_NOTREC:
                $pdfTemplate = 'pdf/search_find_notrec.html.twig';

                break;
            case SearchHelper::SEARCH_STATUS_NOTFIND:
                $pdfTemplate = 'pdf/search_notfind.html.twig';
        }

        $pdfContent = '';
        $fileName = FileHelper::sanitizeFileName(
            $searchLog->getId().'_'.$searchLog->getFirstName().'_'.$searchLog->getName($this->settingsData).'.pdf'
        );

        if ($pdfTemplate) {
            $pdfParameters = [];
            $pdfParameters['reference'] = SearchHelper::getReference($user, $searchLog);

            if ($searchLog->getResponseType() === SearchHelper::SEARCH_STATUS_NOTFIND) {
                $pdfParameters['firstName'] = $searchLog->getFirstName();
                $pdfParameters['name'] = strtoupper($searchLog->getName($this->settingsData));
                $pdfParameters['birthDate'] = $searchLog->getBirthDate();
                $pdfParameters['responseType'] = $searchLog->getResponseType();
            } else {
                $person = $personService->getPerson($searchLog->getPerson());
                $pdfParameters['gender'] = $person->getGender();
                $pdfParameters['firstName'] = $person->getFirstName();
                $pdfParameters['name'] = strtoupper($person->getName($this->settingsData));
                $pdfParameters['birthDate'] = $person->getBirthDate();
                $pdfParameters['responseType'] = $searchLog->getResponseType();

                $fileName = FileHelper::sanitizeFileName(
                    $searchLog->getId().'_'.$person->getFirstName().'_'.$person->getName($this->settingsData).'.pdf'
                );
            }

            $pdfContent = $this->renderView(
                $pdfTemplate,
                $pdfParameters
            );
        }

        $pdfService->create();

        return [$pdfService->generatePdf($pdfContent, $fileName, $dest), $fileName];
    }

    /**
     * Ajoute une recherche dans les logs
     *
     * @param SearchData  $searchData
     * @param int         $responseType
     * @param Person|null $person
     *
     * @return SearchLog
     * @throws \Exception
     */
    private function addSearchLog(SearchData $searchData, int $responseType, ?Person $person): SearchLog
    {
        /** @var User $user */
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();

        $searchLog = $searchData->toSearchLog();
        $searchLog->setUser($user);
        $searchLog->setSearchDate(new \DateTime());
        $searchLog->setResponseType($responseType);
        if ($person) {
            $searchLog->setPerson($person->getPersonNum());
        }

        $entityManager->persist($searchLog);
        $entityManager->flush();

        return $searchLog;
    }

    /**
     * Envoie du mail en résultat d'une recherche
     *
     * @param int                $searchStatus
     * @param MailService        $mailService
     * @param SearchData         $searchData
     * @param array              $instructors
     * @param SearchLog          $searchLog
     * @param PdfService         $pdfService
     * @param PersonService|null $personService
     *
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function sendMail(
        int $searchStatus,
        MailService $mailService,
        SearchData $searchData,
        array $instructors = null,
        SearchLog $searchLog = null,
        PdfService $pdfService = null,
        PersonService $personService = null
    ) {
        /** @var User $user */
        $user = $this->getUser();

        if ($searchData->getRecipient()) {
            $mailSubject = $this->translator->trans(
                'searchResultMail.subject.default',
                [
                    '%appName%'   => $this->settingsData->getAppName(),
                    '%recipient%' => $searchData->getRecipient(),
                    '%firstName%' => $searchData->getFirstName(),
                    '%name%'      => $searchData->getName($this->settingsData),
                ]
            );
        } else {
            $mailSubject = $this->translator->trans(
                'searchResultMail.subject.norecipient',
                [
                    '%appName%'   => $this->settingsData->getAppName(),
                    '%firstName%' => $searchData->getFirstName(),
                    '%name%'      => $searchData->getName($this->settingsData),
                ]
            );
        }

        $mailTemplate = '';
        $mailParameters = [
            'firstName'       => $searchData->getFirstName(),
            'middleName'      => $searchData->getMiddleName(),
            'thirdName'       => $searchData->getThirdName(),
            'useName'         => $searchData->getUseName(),
            'civilName'       => $searchData->getCivilName(),
            'name'            => $searchData->getName($this->settingsData),
            'birthDate'       => $searchData->getBirthDate(),
            'deathDate'       => $searchData->getDeathDate(),
            'libelle_notaire' => $user->getName(),
            'mail_notaire'    => $user->getEmail(),
        ];

        switch ($searchStatus) {
            case SearchHelper::SEARCH_STATUS_FIND_REC:
            case SearchHelper::SEARCH_STATUS_FIND_NOTREC:
                $person = $personService->getPerson($searchLog->getPerson());
                $mailParametersBdd = [
                    'firstNameBdd' => $person->getFirstName(),
                    'nameBdd'      => strtoupper($person->getName($this->settingsData)),
                    'birthDateBdd' => $person->getBirthDate(),
                ];
                $mailParameters = array_merge($mailParameters, $mailParametersBdd);
                $mailTemplate = 'emails/search_find.html.twig';
                break;
            case SearchHelper::SEARCH_STATUS_NOTFIND:
                $mailTemplate = 'emails/search_notfind.html.twig';
                break;
            case SearchHelper::SEARCH_STATUS_AMBIGUOUS:
                $mailTemplate = 'emails/search_ambiguous.html.twig';
                break;
            case SearchHelper::SEARCH_STATUS_HAS_REQUEST:
                $mailSubject = $this->translator->trans(
                    'searchResultMail.subject.hasRequest',
                    [
                        '%appName%' => $this->settingsData->getAppName(),
                    ]
                );
                $mailTemplate = 'emails/search_hasrequest.html.twig';

                break;
        }

        /** @var User $user */
        $user = $this->getUser();

        $mailRecipients = [];
        $mailRecipients[] = $user->getEmail();

        // On envoie le mail aux instructeurs concernés
        if (!empty($instructors)) {
            /** @var Instructor $instructor */
            foreach ($instructors as $instructor) {
                $mailRecipients[] = $instructor->getEmail();
            }
        } elseif ($instructors !== null && $searchStatus !== SearchHelper::SEARCH_STATUS_NOTFIND) {
            $this->addFlash(
                MessageHelper::MESSAGE_WARNING,
                $this->translator->trans('searchResult.noInstructorFound')
            );
        }

        $mailHtml = $this->renderView(
            $mailTemplate,
            $mailParameters
        );

        $pdfAttachments = [];

        if (SearchHelper::hasLetterPdf($searchStatus)) {
            [$pdfContent, $fileName] = $this->getPdf($searchLog, 'S', $pdfService, $personService);
            $pdfAttachments[$fileName] = $pdfContent;
        }

        try {
            $mailService->sendHtmlMail($mailRecipients, $mailSubject, $mailHtml, $pdfAttachments);
        } catch (TransportExceptionInterface $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        } catch (RfcComplianceException $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        }
    }

    /**
     * Recherche d'un instructeur en fonction du résultat de la recherche :
     *  - Type de réponse
     *  - Première lettre du nom de l'individu
     *
     * @param SearchLog     $searchLog
     * @param int           $searchStatus
     * @param PersonService $personService
     *
     * @return array
     */
    private function findInstructorsForPerson(SearchLog $searchLog, int $searchStatus, PersonService $personService)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $instructors = $entityManager->getRepository(Instructor::class)->findAll();
        $validInstructors = [];
        $name = '';

        if ($searchLog->getPerson() > 0) {
            $person = $personService->getPerson($searchLog->getPerson());
            $name = $person->getName($this->settingsData);
        } else {
            $name = $searchLog->getName($this->settingsData);
        }

        /** @var Instructor $instructor */
        foreach ($instructors as $instructor) {
            // Si l'instructeur est affecté pour ce type de réponse
            if (in_array($searchStatus, $instructor->getResponseType())) {
                // L'instructeur est configuré juste pour un Type de réponse (pas d'initiales)
                if (empty($instructor->getInitials())) {
                    $validInstructors[] = $instructor;

                    continue;
                }

                // On test si la personne commence par une des initiales affectées à l'instructeur
                foreach ($instructor->getInitials() as $initial) {
                    if (!$initial) {
                        $validInstructors[] = $instructor;

                        break;
                    }

                    if (preg_match("/^[{$initial}]/i", $name)) {
                        $validInstructors[] = $instructor;

                        break;
                    }
                }
            }
        }

        return $validInstructors;
    }

    /**
     * @param PersonRepository $personRepository
     * @param SearchData       $searchData
     * @param bool             $useSoundex
     *
     * @return array
     */
    private function searchPerson(PersonRepository $personRepository, SearchData $searchData, bool $useSoundex)
    {
        $searchStatus = SearchHelper::SEARCH_STATUS_NOTFIND;
        $person = null;

        // *************************************************************************
        // Passe 1 : Recherche du nombre de réponses seulement avec les conditions :
        // - précise sur noms et prénoms avec suppression des traits d'union
        //     OU précise sur les noms et 3 premières lettres du prénom
        // - contrainte sur date de naissance complète
        // *************************************************************************
        $countPersons = $personRepository->countBySearch($searchData, $useSoundex);

        if ($countPersons > 1) {
            $searchStatus = SearchHelper::SEARCH_STATUS_AMBIGUOUS;
        } elseif ($countPersons === 1) {
            $persons = $personRepository->findBySearch($searchData, $useSoundex);
            $person = $persons[0];

            if ($person->getHelpCode() === SearchHelper::HELP_CODE_REC) {
                // Si le code est 1SEXTREC alors il y a possibilité de récupération

                $searchStatus = SearchHelper::SEARCH_STATUS_FIND_REC;
            } elseif ($person->getHelpCode() === SearchHelper::HELP_CODE_NOTREC) {
                // Si le code est 1SEXTNONR alors il n'y a pas possibilité de récupération

                $searchStatus = SearchHelper::SEARCH_STATUS_FIND_NOTREC;
            }
        } elseif ($countPersons === 0) {
            // *********************************************************************
            // Passe 2 : La requête est relancée sur l'année de naissance seulement
            // *********************************************************************

            $countPersons = $personRepository->countBySearch($searchData, $useSoundex, true);

            if ($countPersons >= 1) {
                $searchStatus = SearchHelper::SEARCH_STATUS_AMBIGUOUS;
            } else {
                // ******************************************************************************************
                // Passe 3 : La requête est relancée sans les prénoms mais avec la date de naissance précise
                // ******************************************************************************************

                $countPersons = $personRepository->countBySearch($searchData, $useSoundex, false, true);

                if ($countPersons >= 1) {
                    $searchStatus = SearchHelper::SEARCH_STATUS_AMBIGUOUS;
                } else {
                    $searchStatus = SearchHelper::SEARCH_STATUS_NOTFIND;
                }
            }
        }

        return [$searchStatus, $person];
    }

    /**
     * Envoie le mail d'alerte décès
     *
     * @param Person      $person
     * @param SearchData  $searchData
     * @param MailService $mailService
     *
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function sendDeathAlertMail(Person $person, SearchData $searchData, MailService $mailService)
    {
        /** @var User $user */
        $user = $this->getUser();
        $mailSubject = $this->translator->trans(
            'searchDeathAlertMail.subject.default',
            [
                '%appName%' => $this->settingsData->getAppName(),
            ]
        );

        $mailTemplate = 'emails/death_alert.html.twig';
        $mailParameters = [
            'userName'      => $user->getName(),
            'userEmail'     => $user->getEmail(),
            'gender'        => $person->getGender(),
            'firstName'     => $person->getFirstName(),
            'useName'       => $person->getUseName(),
            'civilName'     => $person->getCivilName(),
            'birthDate'     => $person->getBirthDate(),
            'deathDate'     => $searchData->getDeathDate(),
            'deathLocation' => $searchData->getDeathLocation(),
        ];

        $mailRecipients = [];
        $mailRecipients[] = $this->settingsData->getDeathAlertMail();

        $mailHtml = $this->renderView(
            $mailTemplate,
            $mailParameters
        );

        if (!empty($mailRecipients)) {
            $mailService->sendHtmlMail($mailRecipients, $mailSubject, $mailHtml);
        }
    }


    /**
     * @param Person      $person
     * @param SearchData  $searchData
     * @param MailService $mailService
     *
     * @return bool
     * @throws TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function sendDeathAlert(?Person $person, SearchData $searchData, MailService $mailService = null)
    {
        if ($person && $this->settingsData->hasDeathAlert() && $this->settingsData->getDeathAlertMail() &&
            $searchData->getDeathDate() !== $person->getDeathDate()) {
            if ($mailService) {
                $this->sendDeathAlertMail($person, $searchData, $mailService);
            }

            return true;
        }

        return false;
    }
}
