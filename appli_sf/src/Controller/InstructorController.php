<?php

namespace App\Controller;

use App\Entity\Instructor;
use App\Form\InstructorType;
use App\Helper\MessageHelper;
use Swift_Mailer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin")
 */
class InstructorController extends BaseController
{
    /**
     * Liste les instructeurs
     *
     * @Route("/instructor/list", name="admin_instructor_list")
     *
     * @return Response
     */
    public function listAction()
    {
        $userRepository = $this->getDoctrine()->getRepository(Instructor::class);

        $instructors = $userRepository->findAll();

        return $this->render(
            'admin/instructor/list.html.twig',
            [
                'instructors' => $instructors,
            ]
        );
    }

    /**
     * Ajoute un instructeur
     *
     * @Route("/instructor/add", name="admin_instructor_add")
     *
     * @param Request             $request
     * @param TranslatorInterface $translator
     *
     * @return Response
     */
    public function addAction(
        Request $request,
        TranslatorInterface $translator
    ) {
        $instructor = new Instructor();

        $form = $this->createForm(InstructorType::class, $instructor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($instructor);
            $entityManager->flush();

            return $this->redirectToRoute('admin_instructor_list');
        }

        return $this->render(
            'admin/instructor/add.html.twig',
            [
                'instructorForm' => $form->createView(),
            ]
        );
    }

    /**
     * Édite un instructeur
     *
     * @Route("/instructor/{id}/edit", name="admin_instructor_edit")
     *
     * @param Instructor          $instructor
     * @param Request             $request
     * @param TranslatorInterface $translator
     *
     * @return Response
     */
    public function editAction(
        Instructor $instructor,
        Request $request,
        TranslatorInterface $translator
    ) {
        $form = $this->createForm(InstructorType::class, $instructor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($instructor);
            $entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('instructorEdit.updated'));

            return $this->redirectToRoute(
                'admin_instructor_list',
                [
                    'id' => $instructor->getId(),
                ]
            );
        }

        return $this->render(
            'admin/instructor/edit.html.twig',
            [
                'instructorForm' => $form->createView(),
            ]
        );
    }

    /**
     * Supprime un instructeur
     *
     * @Route("/instructor/{id}/remove", name="admin_instructor_remove")
     *
     * @param Request             $request
     * @param Instructor          $instructor
     * @param TranslatorInterface $translator
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Request $request, Instructor $instructor, TranslatorInterface $translator)
    {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('delete-instructor', $submittedToken)) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($instructor);
            $entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('instructorDelete.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $translator->trans('instructorDelete.error'));
        }

        return $this->redirectToRoute('admin_instructor_list');
    }
}
