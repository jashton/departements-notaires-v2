<?php

namespace App\Controller;

use App\Entity\Settings;
use App\Entity\UserLog;
use App\Form\SettingsType;
use App\Helper\MessageHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin")
 */
class AdminController extends BaseController
{
    /**
     * Page d'accès aux menu administrateur
     *
     * @Route("/", name="admin")
     */
    public function indexAction()
    {
        return $this->render('admin/index.html.twig', []);
    }

    /**
     * Modifie les paramètres du site
     *
     * @Route("/params", name="admin_settings")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Doctrine\ORM\EntityManagerInterface      $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public function editParamsAction(
        Request $request,
        EntityManagerInterface $em,
        TranslatorInterface $translator
    ) {
        $form = $this->createForm(SettingsType::class, $this->settingsData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $settingsEntities = $this->settingsData->toEntities();

            foreach ($settingsEntities as $settingEntity) {
                $curSettings = $em->getRepository(Settings::class)->findOneBy(['name' => $settingEntity->getName()]);

                if (!$curSettings) {
                    $em->persist($settingEntity);
                } elseif ($settingEntity->getValue()) {
                    $curSettings->setValue($settingEntity->getValue());
                }

                $em->flush();
            }

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('settings.updated'));
        }

        return $this->render(
            'admin/settings.html.twig',
            [
                'settingsForm' => $form->createView(),
            ]
        );
    }

    /**
     * Affiche des logs d'accès
     *
     * @Route("/logs", name="admin_logs")
     */
    public function logsAction()
    {
        $userRepository = $this->getDoctrine()->getRepository(UserLog::class);

        $logs = $userRepository->findAll();

        return $this->render(
            'admin/logs.html.twig',
            [
                'logs' => $logs,
            ]
        );
    }
}
