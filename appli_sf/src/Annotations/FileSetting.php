<?php

namespace App\Annotations;

/**
 * À utiliser pour les paramètres de type fichier à uploader (hors image)
 *
 * @Annotation
 */
class FileSetting
{

}
