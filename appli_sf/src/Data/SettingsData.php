<?php

namespace App\Data;

use App\Annotations\BooleanSetting;
use App\Annotations\DontSave;
use App\Annotations\FileSetting;
use App\Annotations\ImageSetting;
use App\Entity\Settings;
use App\Helper\SearchHelper;
use App\Service\SettingService;
use App\Helper\UploaderHelper;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SettingsData
{
    /**
     * @var string
     */
    protected $appName;

    /**
     * @var string
     */
    protected $appUrl;

    /**
     * @var \Symfony\Component\HttpFoundation\File\File
     * @ImageSetting
     */
    protected $appLogo;

    /**
     * @var \Symfony\Component\HttpFoundation\File\File
     * @ImageSetting
     */
    protected $appFavicon;

    /**
     * @var string
     */
    protected $departmentName;

    /**
     * @var string
     */
    protected $departmentSite;

    /**
     * @var string
     */
    protected $departmentCilSite;

    /**
     * @var bool
     * @BooleanSetting
     */
    protected $soundexSearch = false;

    /**
     * @var integer
     */
    protected $connectionAttempts = 3;

    /**
     * @var int
     */
    protected $searchByName = SearchHelper::SEARCH_BY_USE_NAME;

    /**
     * @var bool
     * @BooleanSetting
     */
    protected $deathAlert = false;

    /**
     * @var string
     */
    protected $deathAlertMail;

    /**
     * @var string
     */
    protected $mailSignature;

    /**
     * @var string
     */
    protected $mailIntroduction;

    /**
     * @var string
     */
    protected $mailNotaBene;

    /**
     * @var string
     */
    protected $pdfDestinationCity;

    /**
     * @var \Symfony\Component\HttpFoundation\File\File
     * @ImageSetting
     */
    protected $pdfLogo1;

    /**
     * @var \Symfony\Component\HttpFoundation\File\File
     * @ImageSetting
     */
    protected $pdfLogo2;

    /**
     * @var \Symfony\Component\HttpFoundation\File\File
     * @ImageSetting
     */
    protected $pdfServiceLogo;

    /**
     * @var \Symfony\Component\HttpFoundation\File\File
     * @ImageSetting
     */
    protected $pdfSignature;

    /**
     * @var string
     */
    protected $pdfSuccessionMail;

    /**
     * @var string
     */
    protected $pdfDepartmentalHouseName;

    /**
     * @var string
     */
    protected $pdfDepartmentalHousePhone;

    /**
     * @var \Symfony\Component\HttpFoundation\File\File
     * @FileSetting
     */
    protected $pdfCSS;

    /**
     * @var SettingService
     * @DontSave
     */
    protected $settingService;

    /**
     * @var \App\Helper\UploaderHelper
     * @DontSave
     */
    protected $uploaderHelper;


    public function __construct(
        SettingService $settingService,
        UploaderHelper $uploaderHelper = null
    ) {
        $this->settingService = $settingService;
        $this->uploaderHelper = $uploaderHelper;
    }

    /**
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public static function getImagesProps()
    {
        $reader = new AnnotationReader();
        $reflClass = new \ReflectionClass(self::class);
        $props = $reflClass->getProperties();
        $imagesProps = [];

        foreach ($props as $prop) {
            $reflProp = new \ReflectionProperty(self::class, $prop->getName());
            $propertyAnnotations = $reader->getPropertyAnnotations($reflProp);

            if (self::hasObjectArray($propertyAnnotations, ImageSetting::class)) {
                $imagesProps[] = $prop->getName();
            }
        }

        return $imagesProps;
    }

    /**
     * @param array  $array
     * @param string $classname
     *
     * @return bool
     */
    private static function hasObjectArray(array $array, string $classname): bool
    {
        foreach ($array as $element) {
            if (($element instanceof $classname)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function getAppName(): ?string
    {
        return $this->appName;
    }

    /**
     * @param string $appName
     *
     * @return SettingsData
     */
    public function setAppName(string $appName): SettingsData
    {
        $this->appName = $appName;

        return $this;
    }

    /**
     * @return string
     */
    public function getAppUrl(): ?string
    {
        return $this->appUrl;
    }

    /**
     * @param string $appUrl
     *
     * @return SettingsData
     */
    public function setAppUrl(string $appUrl): SettingsData
    {
        $this->appUrl = $appUrl;

        return $this;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function getAppLogo(): ?File
    {
        return $this->appLogo;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File $appLogo
     *
     * @return SettingsData
     */
    public function setAppLogo(?File $appLogo): SettingsData
    {
        if ($appLogo) {
            $this->appLogo = $appLogo;
        }

        return $this;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function getAppFavicon(): ?File
    {
        return $this->appFavicon;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File $appFavicon
     *
     * @return SettingsData
     */
    public function setAppFavicon(?File $appFavicon): SettingsData
    {
        if ($appFavicon) {
            $this->appFavicon = $appFavicon;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getDepartmentName(): ?string
    {
        return $this->departmentName;
    }

    /**
     * @param string $departmentName
     *
     * @return SettingsData
     */
    public function setDepartmentName(string $departmentName): SettingsData
    {
        $this->departmentName = $departmentName;

        return $this;
    }

    /**
     * @return string
     */
    public function getDepartmentSite(): ?string
    {
        return $this->departmentSite;
    }

    /**
     * @param string $departmentSite
     *
     * @return SettingsData
     */
    public function setDepartmentSite(string $departmentSite): SettingsData
    {
        $this->departmentSite = $departmentSite;

        return $this;
    }

    /**
     * @return string
     */
    public function getDepartmentCilSite(): ?string
    {
        return $this->departmentCilSite;
    }

    /**
     * @param string $departmentCilSite
     *
     * @return SettingsData
     */
    public function setDepartmentCilSite(string $departmentCilSite): SettingsData
    {
        $this->departmentCilSite = $departmentCilSite;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasSoundexSearch(): ?bool
    {
        return $this->soundexSearch;
    }

    /**
     * @param bool $soundexSearch
     *
     * @return SettingsData
     */
    public function setSoundexSearch(bool $soundexSearch): SettingsData
    {
        $this->soundexSearch = $soundexSearch;

        return $this;
    }

    /**
     * @return int
     */
    public function getConnectionAttempts(): int
    {
        return $this->connectionAttempts;
    }

    /**
     * @param int $connectionAttempts
     *
     * @return SettingsData
     */
    public function setConnectionAttempts(int $connectionAttempts): SettingsData
    {
        $this->connectionAttempts = $connectionAttempts;

        return $this;
    }

    /**
     * @return int
     */
    public function getSearchByName(): int
    {
        return $this->searchByName;
    }

    /**
     * @param int $searchByName
     *
     * @return SettingsData
     */
    public function setSearchByName(int $searchByName): SettingsData
    {
        $this->searchByName = $searchByName;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasDeathAlert(): bool
    {
        return $this->deathAlert;
    }

    /**
     * @param bool $deathAlert
     *
     * @return SettingsData
     */
    public function setDeathAlert(bool $deathAlert): SettingsData
    {
        $this->deathAlert = $deathAlert;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeathAlertMail(): ?string
    {
        return $this->deathAlertMail;
    }

    /**
     * @param string $deathAlertMail
     *
     * @return SettingsData
     */
    public function setDeathAlertMail(string $deathAlertMail): SettingsData
    {
        $this->deathAlertMail = $deathAlertMail;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailSignature(): ?string
    {
        return $this->mailSignature;
    }

    /**
     * @param string $mailSignature
     *
     * @return SettingsData
     */
    public function setMailSignature(string $mailSignature): SettingsData
    {
        $this->mailSignature = $mailSignature;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailIntroduction(): ?string
    {
        return $this->mailIntroduction;
    }

    /**
     * @param string $mailIntroduction
     *
     * @return SettingsData
     */
    public function setMailIntroduction(string $mailIntroduction): SettingsData
    {
        $this->mailIntroduction = $mailIntroduction;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailNotaBene(): ?string
    {
        return $this->mailNotaBene;
    }

    /**
     * @param string $mailNotaBene
     *
     * @return SettingsData
     */
    public function setMailNotaBene(string $mailNotaBene): SettingsData
    {
        $this->mailNotaBene = $mailNotaBene;

        return $this;
    }

    /**
     * @return string
     */
    public function getPdfDestinationCity(): ?string
    {
        return $this->pdfDestinationCity;
    }

    /**
     * @param string $pdfDestinationCity
     *
     * @return SettingsData
     */
    public function setPdfDestinationCity(string $pdfDestinationCity): SettingsData
    {
        $this->pdfDestinationCity = $pdfDestinationCity;

        return $this;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function getPdfLogo1(): ?File
    {
        return $this->pdfLogo1;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File $pdfLogo1
     *
     * @return SettingsData
     */
    public function setPdfLogo1(?File $pdfLogo1): SettingsData
    {
        if ($pdfLogo1) {
            $this->pdfLogo1 = $pdfLogo1;
        }

        return $this;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function getPdfLogo2(): ?File
    {
        return $this->pdfLogo2;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File $pdfLogo2
     *
     * @return SettingsData
     */
    public function setPdfLogo2(?File $pdfLogo2): SettingsData
    {
        if ($pdfLogo2) {
            $this->pdfLogo2 = $pdfLogo2;
        }

        return $this;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function getPdfServiceLogo(): ?File
    {
        return $this->pdfServiceLogo;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File $pdfServiceLogo
     *
     * @return SettingsData
     */
    public function setPdfServiceLogo(?File $pdfServiceLogo): SettingsData
    {
        if ($pdfServiceLogo) {
            $this->pdfServiceLogo = $pdfServiceLogo;
        }

        return $this;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function getPdfSignature(): ?File
    {
        return $this->pdfSignature;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File $pdfSignature
     *
     * @return SettingsData
     */
    public function setPdfSignature(?File $pdfSignature): SettingsData
    {
        if ($pdfSignature) {
            $this->pdfSignature = $pdfSignature;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPdfSuccessionMail(): ?string
    {
        return $this->pdfSuccessionMail;
    }

    /**
     * @param string $pdfSuccessionMail
     *
     * @return SettingsData
     */
    public function setPdfSuccessionMail(string $pdfSuccessionMail): SettingsData
    {
        $this->pdfSuccessionMail = $pdfSuccessionMail;

        return $this;
    }

    /**
     * @return string
     */
    public function getPdfDepartmentalHouseName(): ?string
    {
        return $this->pdfDepartmentalHouseName;
    }

    /**
     * @param string $pdfDepartmentalHouseName
     *
     * @return SettingsData
     */
    public function setPdfDepartmentalHouseName(string $pdfDepartmentalHouseName): SettingsData
    {
        $this->pdfDepartmentalHouseName = $pdfDepartmentalHouseName;

        return $this;
    }

    /**
     * @return string
     */
    public function getPdfDepartmentalHousePhone(): ?string
    {
        return $this->pdfDepartmentalHousePhone;
    }

    /**
     * @param string $pdfDepartmentalHousePhone
     *
     * @return SettingsData
     */
    public function setPdfDepartmentalHousePhone(string $pdfDepartmentalHousePhone): SettingsData
    {
        $this->pdfDepartmentalHousePhone = $pdfDepartmentalHousePhone;

        return $this;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function getPdfCSS(): ?File
    {
        return $this->pdfCSS;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File $pdfCSS
     *
     * @return SettingsData
     */
    public function setPdfCSS(?File $pdfCSS): SettingsData
    {
        if ($pdfCSS) {
            $this->pdfCSS = $pdfCSS;
        }

        return $this;
    }

    /**
     * @return array
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public function toEntities()
    {
        $settingsEntities = [];
        $reader = new AnnotationReader();
        $reflect = new \ReflectionClass($this);
        $objectProps = $reflect->getProperties();

        /** @var \ReflectionProperty $objectProp */
        foreach ($objectProps as $objectProp) {
            $propName = $objectProp->getName();
            $propValue = $this->$propName;
            $reflProp = new \ReflectionProperty($this, $propName);
            $propertyAnnotations = $reader->getPropertyAnnotations($reflProp);

            if (!self::hasObjectArray($propertyAnnotations, DontSave::class)) {
                $setting = new Settings();
                $setting->setName($propName);

                if (self::hasObjectArray($propertyAnnotations, ImageSetting::class)) {
                    if ($propValue instanceof UploadedFile && $this->uploaderHelper !== null) {
                        $file = $this->uploaderHelper->uploadFile($propValue, UploaderHelper::SETTINGS_IMAGE);
                        $filePath = $this->uploaderHelper->getRelativePath($file);

                        $this->$propName = $file;
                        $setting->setValue($filePath);
                    }
                } elseif (self::hasObjectArray($propertyAnnotations, FileSetting::class)) {
                    if ($propValue instanceof UploadedFile && $this->uploaderHelper !== null) {
                        $file = $this->uploaderHelper->uploadFile($propValue, UploaderHelper::SETTINGS_FILE);
                        $filePath = $this->uploaderHelper->getRelativePath($file);

                        $this->$propName = $file;
                        $setting->setValue($filePath);
                    }
                } elseif (self::hasObjectArray($propertyAnnotations, BooleanSetting::class)) {
                    $setting->setValue($propValue ? 'true' : 'false');
                } else {
                    $setting->setValue($propValue);
                }

                $settingsEntities[] = $setting;
            }
        }

        return $settingsEntities;
    }

    /**
     * @return $this
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public function initFromDatabase()
    {
        $reader = new AnnotationReader();
        $reflect = new \ReflectionClass($this);
        $objectProps = $reflect->getProperties();

        /** @var \ReflectionProperty $objectProp */
        foreach ($objectProps as $objectProp) {
            $propName = $objectProp->getName();
            $reflProp = new \ReflectionProperty($this, $propName);
            $propertyAnnotations = $reader->getPropertyAnnotations($reflProp);

            if (!self::hasObjectArray($propertyAnnotations, DontSave::class)) {
                $value = $this->settingService->getValue($objectProp->getName());

                if (!$value) {
                    continue;
                }

                if (self::hasObjectArray($propertyAnnotations, ImageSetting::class)
                    || self::hasObjectArray($propertyAnnotations, FileSetting::class)
                ) {
                    if ($this->uploaderHelper !== null) {
                        $this->$propName = new File($this->uploaderHelper->getServerPath($value));
                    }
                } elseif (self::hasObjectArray($propertyAnnotations, BooleanSetting::class)) {
                    $this->$propName = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                } else {
                    $this->$propName = $value;
                }
            }
        }

        return $this;
    }
}
