<?php

namespace App\Utility;

use App\Repository\UserRepository;
use Rollerworks\Component\PasswordStrength\Validator\Constraints\PasswordRequirements;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Validator
{
    private $translator;
    private $userRepository;
    private $validator;

    public function __construct(
        TranslatorInterface $translator,
        UserRepository $userRepository,
        ValidatorInterface $validator
    ) {
        $this->translator = $translator;
        $this->userRepository = $userRepository;
        $this->validator = $validator;
    }


    /**
     * Valide le nom d'utilisateur
     *
     * @param string|null $username
     *
     * @return string
     */
    public function validateUsername(?string $username): string
    {
        if (empty($username)) {
            throw new InvalidArgumentException(
                $this->translator->trans(
                    'addUser.error.notEmpty',
                    ['%field%' => $this->translator->trans('addUser.login')]
                )
            );
        }

        $user = $this->userRepository->findOneBy(['username' => $username]);

        if ($user) {
            throw new InvalidArgumentException(
                $this->translator->trans('addUser.error.userUsernameExists', ['%username%' => $username])
            );
        }

        return $username;
    }

    /**
     * Valide l'email
     *
     * @param string|null $email
     *
     * @return string
     */
    public function validateEmail(?string $email): string
    {
        if (empty($email)) {
            throw new InvalidArgumentException(
                $this->translator->trans(
                    'addUser.error.notEmpty',
                    ['%field%' => $this->translator->trans('addUser.email')]
                )
            );
        }

        $userEmail = $this->userRepository->findOneBy(['email' => $email]);

        if ($userEmail) {
            throw new InvalidArgumentException(
                $this->translator->trans('addUser.error.userEmailExists', ['%email%' => $email])
            );
        }

        return $email;
    }

    /**
     * Valide le mot de passe
     *
     * @param string|null $plainPassword
     *
     * @return string
     */
    public function validatePassword(?string $plainPassword): string
    {
        if (empty($plainPassword)) {
            throw new InvalidArgumentException(
                $this->translator->trans(
                    'addUser.error.notEmpty',
                    ['%field%' => $this->translator->trans('addUser.password')]
                )
            );
        }

        $passwordConstraint = new PasswordRequirements(
            [
                'minLength'               => 12,
                'requireCaseDiff'         => true,
                'requireNumbers'          => true,
                'requireSpecialCharacter' => true,
            ]
        );

        $errors = $this->validator->validate(
            $plainPassword,
            $passwordConstraint
        );

        if (count($errors) > 0) {
            $messages = [];

            foreach ($errors as $error) {
                $messages[] = $error->getMessage();
            }

            throw new InvalidArgumentException(implode("\n", $messages));
        }

        return $plainPassword;
    }

    /**
     * Valide si non vide
     *
     * @param string|null $value
     *
     * @param string      $name
     *
     * @return string
     */
    public function validateNotEmpty(?string $value, $name = null): string
    {
        if (empty($value)) {
            throw new InvalidArgumentException(
                $name ?
                    $this->translator->trans(
                        'addUser.error.notEmptyFieldName',
                        ['%fieldName%' => $this->translator->trans('addUser.'.$name)]
                    ) :
                    $this->translator->trans(
                        'addUser.error.notEmpty'
                    )
            );
        }

        return $value;
    }
}
