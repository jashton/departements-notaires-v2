<?php

namespace App\Service;

use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Exception\RfcComplianceException;

class MailService
{
    private $mailer;
    private $mailSender;

    public function __construct(MailerInterface $mailer, string $mailSender)
    {
        $this->mailer = $mailer;
        $this->mailSender = $mailSender;
    }

    /**
     * Envoie d'un mail
     *
     * @param array  $recipients
     * @param string $subject
     * @param string $html
     * @param array  $pdfAttachments
     *
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws RfcComplianceException
     */
    public function sendHtmlMail(array $recipients, string $subject, string $html, array $pdfAttachments = [])
    {
        try {
            $email = (new Email())
                ->from($this->mailSender)
                ->subject($subject)
                ->html($html);
        } catch (RfcComplianceException $exception) {
            throw $exception;
        }

        foreach ($pdfAttachments as $name => $attachment) {
            $email->attach($attachment, $name, 'application/pdf');
        }

        foreach ($recipients as $recipient) {
            $email->addTo($recipient);
        }

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $exception) {
            throw $exception;
        }
    }
}
