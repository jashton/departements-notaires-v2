<?php

namespace App\Service;

use App\Data\SettingsData;

/**
 * Class SettingService.
 */
class SettingsDataService
{

    /**
     * @var SettingsData
     */
    private $settingsData;

    /**
     * SettingDataService constructor.
     *
     * @param SettingsData $settingsData
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public function __construct(SettingsData $settingsData)
    {
        $this->settingsData = $settingsData->initFromDatabase();
    }

    /**
     * Récupère les paramètres du site
     *
     * @return SettingsData
     */
    public function getData(): SettingsData
    {
        return $this->settingsData;
    }
}
