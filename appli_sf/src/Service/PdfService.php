<?php

namespace App\Service;

use Spipu\Html2Pdf\Html2Pdf;

class PdfService
{
    /**
     * @var Html2Pdf
     */
    private $pdf;

    /**
     * @var string
     */
    private $pathPdf;

    /**
     * @var string
     */
    private $orientation = 'P';

    /**
     * @var string
     */
    private $format = 'A4';

    /**
     * @var string
     */
    private $lang = 'fr';

    /**
     * @var boolean
     */
    private $unicode = true;

    /**
     * @var string
     */
    private $encoding = 'UTF-8';

    /**
     * @var array
     */
    private $margin = [0, 0, 0, 5];

    /**
     * Création du PDF
     *
     * @param string|null $orientation
     * @param string|null $format
     * @param string|null $lang
     * @param bool|null   $unicode
     * @param string|null $encoding
     * @param array|null  $margin
     */
    public function create(
        string $orientation = null,
        string $format = null,
        string $lang = null,
        bool $unicode = null,
        string $encoding = null,
        array $margin = null
    ) {
        $this->pdf = new Html2Pdf(
            $orientation ? $orientation : $this->orientation,
            $format ? $format : $this->format,
            $lang ? $lang : $this->lang,
            $unicode ? $unicode : $this->unicode,
            $encoding ? $encoding : $this->encoding,
            $margin ? $margin : $this->margin
        );
    }

    /**
     * Génère un PDF à partir du template HTML
     * Destinations possibles :
     *  - I : envoyé dans le navigateur
     *  - D : envoyé dans le navigateur avec téléchargement forcé
     *  - F : sauvegarde du fichier sur le serveur
     *  - S : retourne le document dans une string
     *  - FI: équivalent à F + I
     *  - FD: équivalent à F + D
     *  - E : retourne le document en tant que pièce jointe d'email MIME multi-partie en base64 (RFC 2045)
     *
     * @param string $template Contenu HTML
     * @param string $name     Nom du fichier
     * @param string $dest     Destination où envoyer le fichier
     *
     * @return string
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     */
    public function generatePdf(string $template, string $name, string $dest): string
    {
        $this->pdf->writeHTML($template);

        return $this->pdf->output($name, $dest);
    }
}
