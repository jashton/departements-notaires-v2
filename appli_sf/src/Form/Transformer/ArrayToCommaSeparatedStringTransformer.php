<?php

namespace App\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

class ArrayToCommaSeparatedStringTransformer implements DataTransformerInterface
{

    public function transform($array)
    {
        if ($array) {
            return implode(',', $array);
        } else {
            return '';
        }
    }

    public function reverseTransform($string)
    {
        return explode(',', $string);
    }
}
