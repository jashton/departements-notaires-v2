<?php

namespace App\Form;

use App\Entity\User;
use App\Helper\UserHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'addUser.email',
                ]
            )
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'addUser.name',
                ]
            )
            ->add(
                'roles',
                ChoiceType::class,
                [
                    'choices' => UserHelper::getRolesChoices(),
                    'label'   => 'addUser.role',
                ]
            )
            ->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'addUser.send',
                ]
            );

        $builder->get('roles')
            ->addModelTransformer(
                new CallbackTransformer(
                    function ($rolesAsArray) {
                        if ($rolesAsArray) {
                            return implode(', ', $rolesAsArray);
                        } else {
                            return '';
                        }
                    },
                    function ($rolesAsString) {
                        return explode(', ', $rolesAsString);
                    }
                )
            );

        $formModifier = function (FormInterface $form, ?array $roles = []) {
            if ($roles && count($roles) > 0) {
                switch ($roles[0]) {
                    case UserHelper::ROLE_ADMIN:
                        $this->removeFields($form, ['service', 'address', 'additionalAddress', 'postalCode', 'town']);

                        $form->add(
                            'username',
                            TextType::class,
                            [
                                'label' => 'addUser.login',
                            ]
                        );

                        break;
                    case UserHelper::ROLE_AGENT:
                        $this->removeFields($form, ['address', 'additionalAddress', 'postalCode', 'town']);

                        $form->add(
                            'username',
                            TextType::class,
                            [
                                'label' => 'addUser.login',
                            ]
                        )->add(
                            'service',
                            TextType::class,
                            [
                                'label' => 'addUser.service',
                            ]
                        );

                        break;
                    case UserHelper::ROLE_NOTARY:
                        $this->removeFields($form, ['service']);

                        $form->add(
                            'username',
                            TextType::class,
                            [
                                'label' => 'addUser.codecrpcen',
                            ]
                        )
                            ->add(
                                'address',
                                TextType::class,
                                [
                                    'label' => 'addUser.address',
                                ]
                            )
                            ->add(
                                'additionalAddress',
                                TextType::class,
                                [
                                    'label'    => 'addUser.additionalAddress',
                                    'required' => false,
                                ]
                            )
                            ->add(
                                'postalCode',
                                TextType::class,
                                [
                                    'label' => 'addUser.postalCode',
                                ]
                            )
                            ->add(
                                'town',
                                TextType::class,
                                [
                                    'label' => 'addUser.town',
                                ]
                            );

                        break;
                }
            } else {
                $form->add(
                    'username',
                    TextType::class,
                    [
                        'label' => 'addUser.login',
                    ]
                );
            }
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getRoles());
            }
        );

        $builder->get('roles')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $roles = $event->getForm()->getData();

                $formModifier($event->getForm()->getParent(), $roles);
            }
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
            ]
        );
    }

    /**
     * @param FormInterface $form
     * @param array         $fieldsToRemove
     */
    private function removeFields(FormInterface &$form, array $fieldsToRemove)
    {
        foreach ($fieldsToRemove as $field) {
            if ($form->has($field)) {
                $form->remove($field);
            }
        }
    }
}
