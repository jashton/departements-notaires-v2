<?php

namespace App\Form;

use App\Entity\Instructor;
use App\Form\Transformer\ArrayToCommaSeparatedStringTransformer;
use App\Helper\SearchHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class InstructorType extends AbstractType
{
    private $transformer;

    public function __construct(ArrayToCommaSeparatedStringTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'firstName',
                TextType::class,
                [
                    'label' => 'addInstructor.firstName',
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'label' => 'addInstructor.lastName',
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'addInstructor.email',
                ]
            )
            ->add(
                'phone',
                TextType::class,
                [
                    'label'    => 'addInstructor.phone',
                    'required' => false,
                ]
            )
            ->add(
                'responseType',
                ChoiceType::class,
                [
                    'choices'     => SearchHelper::getResponseTypesChoicesForInstructor(),
                    'label'       => 'addInstructor.responseType',
                    'multiple'    => true,
                    'expanded'    => true,
                    'constraints' => [new NotBlank()],
                ]
            )
            ->add(
                'initials',
                TextType::class,
                [
                    'label'    => 'addInstructor.initials',
                    'required' => false,
                    'help'     => 'fieldHelp.initials',
                ]
            )
            ->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'addUser.send',
                ]
            );

        $builder->get('initials')->addModelTransformer($this->transformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Instructor::class,
            ]
        );
    }
}
