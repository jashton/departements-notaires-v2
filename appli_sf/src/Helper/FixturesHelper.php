<?php

namespace App\Helper;

class FixturesHelper
{
    const GROUP_USE_NAME = 'use_name';
    const GROUP_CIVIL_NAME = 'civil_name';
    const GROUP_USERS = 'users';
}
