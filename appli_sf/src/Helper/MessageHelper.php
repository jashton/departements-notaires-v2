<?php

namespace App\Helper;

class MessageHelper
{
    const MESSAGE_ERROR   = 'danger';
    const MESSAGE_WARNING = 'warning';
    const MESSAGE_SUCCESS = 'success';
    const MESSAGE_INFO    = 'info';
}
