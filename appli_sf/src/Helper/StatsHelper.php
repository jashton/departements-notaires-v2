<?php

namespace App\Helper;

use Symfony\Contracts\Translation\TranslatorInterface;

class StatsHelper
{
    /**
     * Transforme le tableau de stats pour l'affichage en HTML
     * TODO : Peut-être trouver un truc plus "joli"
     *
     * @param array               $stats
     * @param TranslatorInterface $translator
     *
     * @return array
     */
    public static function verticalToHorizontalStatsArray(array $stats, TranslatorInterface $translator): array
    {
        $horizontalStats = [
            0 => [$translator->trans('stats.nbRequests')],
            1 => [$translator->trans('stats.nbResponses')],
            2 => [$translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_FIND_REC)],
            3 => [$translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_FIND_NOTREC)],
            4 => [$translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_AMBIGUOUS)],
            5 => [$translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_NOTFIND)],
            6 => [$translator->trans('stats.nbConnections')],
            7 => [$translator->trans('stats.nbUniqueUsers')],
        ];

        foreach ($stats as $monthStat) {
            $horizontalStats[0][] = $monthStat['nbRequests'];
            $horizontalStats[1][] = $monthStat['nbResponses'];
            $horizontalStats[2][] = $monthStat['nbResponsesByType'][SearchHelper::SEARCH_STATUS_FIND_REC];
            $horizontalStats[3][] = $monthStat['nbResponsesByType'][SearchHelper::SEARCH_STATUS_FIND_NOTREC];
            $horizontalStats[4][] = $monthStat['nbResponsesByType'][SearchHelper::SEARCH_STATUS_AMBIGUOUS];
            $horizontalStats[5][] = $monthStat['nbResponsesByType'][SearchHelper::SEARCH_STATUS_NOTFIND];
            $horizontalStats[6][] = $monthStat['nbConnections'];
            $horizontalStats[7][] = $monthStat['nbUniqueUsers'];
        }

        return $horizontalStats;
    }
}
