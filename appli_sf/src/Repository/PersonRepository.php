<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    /**
     * Compte le nombre de résultats de recherche
     *
     * @param SearchData $data             Données de recherche
     * @param bool       $useSoundex       Utiliser soundex
     * @param bool       $onlyBirthYear    Utiliser seulement l'année de naissance
     * @param bool       $withoutFirstName Ne pas utiliser le prénom
     * @param bool       $onlyNullHelpCode Individu sans code d'aide (demande en cours)
     *
     * @return int Nombre de résultats de recherche
     */
    public function countBySearch(
        SearchData $data,
        bool $useSoundex,
        bool $onlyBirthYear = false,
        bool $withoutFirstName = false,
        bool $onlyNullHelpCode = false
    ) {
        $queryBuilder = $this->createQueryBuilder('p');

        $queryBuilder->select('COUNT(DISTINCT p.id)');

        if ($useSoundex) {
            return $this->searchWithSoundex(
                $queryBuilder,
                $data,
                $onlyBirthYear,
                $withoutFirstName,
                $onlyNullHelpCode
            )->getSingleScalarResult();
        } else {
            return $this->search(
                $queryBuilder,
                $data,
                $onlyBirthYear,
                $withoutFirstName,
                $onlyNullHelpCode
            )->getSingleScalarResult();
        }
    }

    /**
     * Retourne les individus trouvé par la recherche
     *
     * @param SearchData $data             Données de recherche
     * @param bool       $useSoundex       Utiliser soundex
     * @param bool       $onlyBirthYear    Utiliser seulement l'année de naissance
     * @param bool       $withoutFirstName Ne pas utiliser le prénom
     * @param bool       $onlyNullHelpCode Individu sans code d'aide (demande en cours)
     *
     * @return Person[]
     */
    public function findBySearch(
        SearchData $data,
        bool $useSoundex,
        bool $onlyBirthYear = false,
        bool $withoutFirstName = false,
        bool $onlyNullHelpCode = false
    ) {
        $queryBuilder = $this->createQueryBuilder('p');

        if ($useSoundex) {
            return $this->searchWithSoundex(
                $queryBuilder,
                $data,
                $onlyBirthYear,
                $withoutFirstName,
                $onlyNullHelpCode
            )->getResult();
        } else {
            return $this->search(
                $queryBuilder,
                $data,
                $onlyBirthYear,
                $withoutFirstName,
                $onlyNullHelpCode
            )->getResult();
        }
    }

    /**
     * Recherche avec soundex
     *
     * @param QueryBuilder $queryBuilder
     * @param SearchData   $data             Données de recherche
     * @param bool         $onlyBirthYear    Utiliser seulement l'année de naissance
     * @param bool         $withoutFirstName Ne pas utiliser le prénom
     * @param bool         $onlyNullHelpCode Individu sans code d'aide (demande en cours)
     *
     * @return mixed
     */
    private function searchWithSoundex(
        QueryBuilder &$queryBuilder,
        SearchData $data,
        bool $onlyBirthYear = false,
        bool $withoutFirstName = false,
        bool $onlyNullHelpCode = false
    ) {
        $parameters = [];

        $lastNames = $queryBuilder->expr()->orX(
            $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:useName)'),
            $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:civilName)'),
            $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:useName)'),
            $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:civilName)')
        );

        $parameters['useName'] = $data->getUseName();
        $parameters['civilName'] = $data->getCivilName();

        if ($withoutFirstName) {
            $orNames = $lastNames;
        } else {
            $orNames = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->andX(
                    $lastNames,
                    $queryBuilder->expr()->orX(
                        $queryBuilder->expr()->eq('SOUNDEX(p.firstName)', 'SOUNDEX(:firstName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.firstName)', 'SOUNDEX(:middleName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.firstName)', 'SOUNDEX(:thirdName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.middleName)', 'SOUNDEX(:firstName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.middleName)', 'SOUNDEX(:middleName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.middleName)', 'SOUNDEX(:thirdName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.thirdName)', 'SOUNDEX(:firstName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.thirdName)', 'SOUNDEX(:middleName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.thirdName)', 'SOUNDEX(:thirdName)')
                    )
                ),
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->orX(
                        $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:useName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:civilName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:useName)'),
                        $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:civilName)')
                    ),
                    $queryBuilder->expr()->like('p.firstName', ':firstNameStart')
                )
            );

            $parameters['firstName'] = $data->getFirstName();
            $parameters['middleName'] = $data->getMiddleName();
            $parameters['thirdName'] = $data->getThirdName();
            $parameters['firstNameStart'] = $data->getFirstNameStart().'%';
        }

        if ($onlyBirthYear) {
            $birthDay = $queryBuilder->expr()->eq('YEAR(p.birthDate)', ':birthDate');
            $birthDayFormat = 'Y';
        } else {
            $birthDay = $queryBuilder->expr()->eq('p.birthDate', ':birthDate');
            $birthDayFormat = 'Y-m-d';
        }

        $parameters['birthDate'] = $data->getBirthDate()->format($birthDayFormat);

        if ($onlyNullHelpCode) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->isNull('p.helpCode'),
                    $queryBuilder->expr()->eq('p.helpCode', "''")
                )
            );
        } else {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->isNotNull('p.helpCode'),
                    $queryBuilder->expr()->neq('p.helpCode', "''")
                )
            );
        }

        return $queryBuilder
            ->andWhere($orNames)
            ->andWhere($birthDay)
            ->setParameters(
                $parameters
            )
            ->getQuery();
    }

    /**
     * Recherche sans soundex
     *
     * @param QueryBuilder $queryBuilder
     * @param SearchData   $data             Données de recherche
     * @param bool         $onlyBirthYear    Utiliser seulement l'année de naissance
     * @param bool         $withoutFirstName Ne pas utiliser le prénom
     * @param bool         $onlyNullHelpCode Individu sans code d'aide (demande en cours)
     *
     * @return mixed
     */
    private function search(
        QueryBuilder &$queryBuilder,
        SearchData $data,
        bool $onlyBirthYear = false,
        bool $withoutFirstName = false,
        bool $onlyNullHelpCode = false
    ) {
        $parameters = [];
        $lastNames = $queryBuilder->expr()->orX(
            $queryBuilder->expr()->in('p.useName', ':namesWithoutHyphens'),
            $queryBuilder->expr()->in('p.civilName', ':namesWithoutHyphens')
        );

        $parameters['namesWithoutHyphens'] = $data->getAllNamesWithoutHyphens();

        if ($withoutFirstName) {
            $orNames = $lastNames;
        } else {
            $orNames = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->andX(
                    $lastNames,
                    $queryBuilder->expr()->orX(
                        $queryBuilder->expr()->in('p.firstName', ':firstNames'),
                        $queryBuilder->expr()->in('p.middleName', ':firstNames'),
                        $queryBuilder->expr()->in('p.thirdName', ':firstNames')
                    )
                ),
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->orX(
                        $queryBuilder->expr()->in('p.useName', ':names'),
                        $queryBuilder->expr()->in('p.civilName', ':names')
                    ),
                    $queryBuilder->expr()->like('p.firstName', ':firstNameStart')
                )
            );

            $parameters['names'] = $data->getAllNames();
            $parameters['firstNames'] = $data->getAllFirstNames();
            $parameters['firstNameStart'] = $data->getFirstNameStart().'%';
        }

        if ($onlyBirthYear) {
            $birthDay = $queryBuilder->expr()->eq('YEAR(p.birthDate)', ':birthDate');
            $birthDayFormat = 'Y';
        } else {
            $birthDay = $queryBuilder->expr()->eq('p.birthDate', ':birthDate');
            $birthDayFormat = 'Y-m-d';
        }

        $parameters['birthDate'] = $data->getBirthDate()->format($birthDayFormat);

        if ($onlyNullHelpCode) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->isNull('p.helpCode'),
                    $queryBuilder->expr()->like('p.helpCode', "''")
                )
            );
        } else {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->isNotNull('p.helpCode'),
                    $queryBuilder->expr()->notLike('p.helpCode', "''")
                )
            );
        }

        return $queryBuilder
            ->andWhere($orNames)
            ->andWhere($birthDay)
            ->setParameters(
                $parameters
            )
            ->getQuery();
    }
}
