"use strict";

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.scss';

import Cleave from 'cleave.js';

require('better-dom/dist/better-dom');
require('better-dateinput-polyfill/dist/better-dateinput-polyfill');

const tablesort = require('tablesort');

window.onload = function () {
    let header = document.getElementsByClassName('header')[0],
        body = document.getElementsByTagName('body')[0],
        menu_hamburger = document.getElementsByClassName('hamburger')[0];

    function menuClick(event) {
        event.preventDefault();

        body.classList.toggle('is-fixed');
        menu_hamburger.classList.toggle('is-active');
        header.classList.toggle('is-mobile-nav-open');
    }

    menu_hamburger.addEventListener('click', menuClick, false);

    // Is Mobile ?
    function checkMobile() {
        let mHamburgerStyle = getComputedStyle(menu_hamburger);

        if (mHamburgerStyle.display === 'none') {
            menu_hamburger.classList.remove('is-active');
            header.classList.remove('is-mobile-nav-open');
            body.classList.remove('body-fixed');
        }
    }

    checkMobile();
    window.addEventListener('resize', function () {
        checkMobile();
    });

    // Changement header au scroll
    let navTop = document.getElementsByClassName('nav-top')[0];
    let navMiddle = document.getElementsByClassName('nav-middle')[0];
    let styleNavTop = getComputedStyle(navTop);
    let styleNavMiddle = getComputedStyle(navMiddle);
    let navHeight = navTop.offsetHeight + parseInt(styleNavTop.marginTop) + parseInt(styleNavTop.marginBottom) +
        navMiddle.offsetHeight + parseInt(styleNavMiddle.marginTop) + parseInt(styleNavMiddle.marginBottom) + 1;

    isScrolled(header, navTop, navHeight, this);

    window.addEventListener('scroll', function () {
        isScrolled(header, navTop, navHeight, this);
    });

    // Responsive bouton "Aide" nav
    function helpButton() {
        let navHelp = document.getElementsByClassName('nav-help')[0];
        let style = getComputedStyle(navHelp);
        let helpWidth = navHelp.offsetWidth + parseInt(style.paddingLeft) + parseInt(style.paddingRight);
        let helpSpace = (window.innerWidth - 1050) / 2;

        if (helpSpace <= helpWidth) {
            navHelp.classList.add('is-static');
        } else {
            navHelp.classList.remove('is-static');
        }
    }

    helpButton();
    window.addEventListener('resize', function () {
        helpButton();
    });

    // Margin-top main par rapport à la hauteur de la nav
    function mainMargin() {
        let headerHeight = header.offsetHeight;
        let main = document.getElementsByTagName('main')[0];

        if (main) {
            main.style.marginTop = headerHeight + 'px';
        }
    }

    mainMargin();
    window.addEventListener('resize', function () {
        mainMargin();
    });

    // Responsive boutons nav supérieure
    function navTopResp() {
        let navTopSpan = document.querySelectorAll('.nav-top .nav-action span');

        if (window.matchMedia('(max-width: 520px)').matches) {
            navTopSpan.forEach(function (item) {
                item.classList.add('is-invisible');
            });
        } else {
            navTopSpan.forEach(function (item) {
                item.classList.remove('is-invisible');
            });
        }
    }

    navTopResp();
    window.addEventListener('resize', function () {
        navTopResp();
    });

    // Animation boutons switch
    let switchButtons = document.querySelectorAll('.switch');

    switchButtons.forEach(function (switchButton) {
        switchButton.addEventListener('click', function () {
            this.classList.toggle('switch-off')
        });
    });

    // Scroll smooth aux ancres
    const linksToAnchors = document.querySelectorAll('a[href^="#"]');

    linksToAnchors.forEach(each => (each.onclick = anchorLinkHandler));

    // Tri des en cliquant sur l'entête d'une colonne
    let tablesToSort = document.getElementsByClassName('table-sort');

    tablesToSort.forEach(function (table) {
        tablesort(table);
    });

    // Formats dans les champs de formulaires
    document.querySelectorAll('.input-date').forEach(function (el) {
        new Cleave(el, {
            date:        true,
            datePattern: ['d', 'm', 'Y']
        });
    });

    // Alert validation formulaire
    document.querySelectorAll('.show-alert').forEach(function (form) {
        form.addEventListener('submit', function (event) {
            event.preventDefault();

            if (confirm(form.dataset.alertMsg)) {
                form.submit();
            }
        });
    });

    // Afficher/cacher un champ
    document.querySelectorAll('.input-wrapper[data-id]').forEach(function (wrapper) {
        wrapper.dataset.display = window.getComputedStyle(wrapper).display;
        wrapper.style.display = 'none';
    });

    document.querySelectorAll('a.show-field').forEach(function (elem) {
        elem.innerHTML = elem.dataset.plus;
        elem.addEventListener('click', showFieldClick, false);
    })

    function showFieldClick(event) {
        event.preventDefault();
        let elem = event.target;
        let id = elem.dataset.target;

        document.querySelectorAll('.input-wrapper[data-id=' + id + ']').forEach(function (wrapper) {
            if (window.getComputedStyle(wrapper).display !== 'none') {
                wrapper.style.display = 'none'
                elem.innerHTML = elem.dataset.plus;
            } else {
                wrapper.style.display = wrapper.dataset.display;
                elem.innerHTML = elem.dataset.minus;
            }
        });
    }
};

function anchorLinkHandler(e) {
    const distanceToTop = el => Math.floor(el.getBoundingClientRect().top);

    e.preventDefault();
    const targetID = this.getAttribute("href");
    const targetAnchor = document.querySelector(targetID);
    if (!targetAnchor) return;
    const originalTop = distanceToTop(targetAnchor);

    window.scrollBy({top: originalTop, left: 0, behavior: "smooth"});

    const checkIfDone = setInterval(function () {
        const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
        if (distanceToTop(targetAnchor) === 0 || atBottom) {
            targetAnchor.tabIndex = "-1";
            targetAnchor.focus();
            window.history.pushState("", "", targetID);
            clearInterval(checkIfDone);
        }
    }, 100);
}

function isScrolled(header, navTop, navHeight, curElem) {
    header.classList.toggle('is-scrolled', curElem.scrollY > 10);

    if (header.classList.contains('is-scrolled')) {
        navTop.style.marginTop = '-' + navHeight + 'px';
    } else {
        navTop.style.marginTop = 0;
    }
}
