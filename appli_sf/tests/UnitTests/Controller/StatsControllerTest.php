<?php

namespace App\Tests\UnitTests\Controller;

use App\Helper\SearchHelper;
use App\Helper\StatsHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class StatsControllerTest extends KernelTestCase
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->translator = $kernel->getContainer()->get('translator');
    }

    /**
     * Teste la fonction pour transformer un tableau
     */
    public function testVerticalToHorizontalStatsArray()
    {
        $givenData = [
            '2020-06' =>
                [
                    'nbRequests'        => 5,
                    'nbResponses'       => 5,
                    'nbResponsesByType' =>
                        [
                            1 => 2,
                            2 => 0,
                            4 => 1,
                            3 => 2,
                        ],
                    'nbConnections'     => 22,
                    'nbUniqueUsers'     => 13,
                ],
            '2020-05' =>
                [
                    'nbRequests'        => 85,
                    'nbResponses'       => 85,
                    'nbResponsesByType' =>
                        [
                            1 => 50,
                            2 => 2,
                            4 => 9,
                            3 => 23,
                            0 => 1,
                        ],
                    'nbConnections'     => 26,
                    'nbUniqueUsers'     => 2,
                ],
            '2020-04' =>
                [
                    'nbRequests'        => 7,
                    'nbResponses'       => 7,
                    'nbResponsesByType' =>
                        [
                            1 => 6,
                            2 => 0,
                            4 => 0,
                            3 => 1,
                        ],
                    'nbConnections'     => 21,
                    'nbUniqueUsers'     => 3,
                ],
            '2020-03' =>
                [
                    'nbRequests'        => 28,
                    'nbResponses'       => 20,
                    'nbResponsesByType' =>
                        [
                            1 => 5,
                            2 => 7,
                            4 => 3,
                            3 => 5,
                        ],
                    'nbConnections'     => 56,
                    'nbUniqueUsers'     => 10,
                ],
            '2020-02' =>
                [
                    'nbRequests'        => 19,
                    'nbResponses'       => 19,
                    'nbResponsesByType' =>
                        [
                            1 => 7,
                            2 => 11,
                            4 => 1,
                            3 => 0,
                        ],
                    'nbConnections'     => 31,
                    'nbUniqueUsers'     => 5,
                ],
        ];

        $returnData = StatsHelper::verticalToHorizontalStatsArray($givenData, $this->translator);

        $expectedData = [
            0 =>
                [
                    0 => $this->translator->trans('stats.nbRequests'),
                    1 => 5,
                    2 => 85,
                    3 => 7,
                    4 => 28,
                    5 => 19,
                ],
            1 =>
                [
                    0 => $this->translator->trans('stats.nbResponses'),
                    1 => 5,
                    2 => 85,
                    3 => 7,
                    4 => 20,
                    5 => 19,
                ],
            2 =>
                [
                    0 => $this->translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_FIND_REC),
                    1 => 2,
                    2 => 50,
                    3 => 6,
                    4 => 5,
                    5 => 7,
                ],
            3 =>
                [
                    0 => $this->translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_FIND_NOTREC),
                    1 => 0,
                    2 => 2,
                    3 => 0,
                    4 => 7,
                    5 => 11,
                ],
            4 =>
                [
                    0 => $this->translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_AMBIGUOUS),
                    1 => 1,
                    2 => 9,
                    3 => 0,
                    4 => 3,
                    5 => 1,
                ],
            5 =>
                [
                    0 => $this->translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_NOTFIND),
                    1 => 2,
                    2 => 23,
                    3 => 1,
                    4 => 5,
                    5 => 0,
                ],
            6 =>
                [
                    0 => $this->translator->trans('stats.nbConnections'),
                    1 => 22,
                    2 => 26,
                    3 => 21,
                    4 => 56,
                    5 => 31,
                ],
            7 =>
                [
                    0 => $this->translator->trans('stats.nbUniqueUsers'),
                    1 => 13,
                    2 => 2,
                    3 => 3,
                    4 => 10,
                    5 => 5,
                ],
        ];

        static::assertEquals($expectedData, $returnData);
    }
}
