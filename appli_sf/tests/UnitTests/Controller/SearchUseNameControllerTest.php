<?php

namespace App\Tests\UnitTests\Controller;

use App\Data\SettingsData;
use App\DataFixtures\PersonUseNameFixtures;
use App\DataFixtures\UserFixtures;
use App\Helper\SearchHelper;
use App\Service\SettingsDataService;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SearchUseNameControllerTest extends AbstractSearchControllerTest
{
    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->translator = $kernel->getContainer()->get('translator');
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->settingService = $kernel->getContainer()->get('setting');
        $encoder = self::$container->get(UserPasswordEncoderInterface::class);

        // Chargement des fixtures
        $this->addFixture(new UserFixtures($encoder));
        $this->addFixture(new PersonUseNameFixtures());
        $this->executeFixtures();

        $this->settingsData = new SettingsData($this->settingService);
    }

    /**
     * Initialise les paramètres de l'application
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    protected function initSettingsDataService()
    {
        $this->settingsData->setAppName('D&N');
        $this->settingsData->setSearchByName(SearchHelper::SEARCH_BY_USE_NAME);

        $this->settingsDataService = new SettingsDataService($this->settingsData);
    }
}
