<?php

namespace App\Tests\FunctionalTests\Controller;

use App\Entity\User;
use App\Helper\UserHelper;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultControllerTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser|null
     */
    private $client = null;

    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * Teste affichage page de login
     */
    public function testLoginPageIsUp()
    {
        $this->client->request('GET', '/login');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page d'aide
     */
    public function testHelpPageIsUp()
    {
        $this->client->request('GET', '/help');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page CGU
     */
    public function testTermsOfUsePageIsUp()
    {
        $this->logIn(UserHelper::ROLE_NOTARY);
        $this->client->request('GET', '/terms_of_use');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page menu admin
     */
    public function testAdminMenuPageIsUp()
    {
        $this->logIn(UserHelper::ROLE_ADMIN);
        $this->client->request('GET', '/admin/');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );

        $this->logIn(UserHelper::ROLE_AGENT);
        $this->client->request('GET', '/admin/');

        static::assertSame(
            Response::HTTP_FORBIDDEN,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page des paramètres
     */
    public function testSettingsPageIsUp()
    {
        $this->logIn(UserHelper::ROLE_ADMIN);
        $this->client->request('GET', '/admin/params');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );

        $this->logIn(UserHelper::ROLE_AGENT);
        $this->client->request('GET', '/admin/params');

        static::assertSame(
            Response::HTTP_FORBIDDEN,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page logs d'accès
     */
    public function testAccessLogsPageIsUp()
    {
        $this->logIn(UserHelper::ROLE_ADMIN);
        $this->client->request('GET', '/admin/logs');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );

        $this->logIn(UserHelper::ROLE_AGENT);
        $this->client->request('GET', '/admin/logs');

        static::assertSame(
            Response::HTTP_FORBIDDEN,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page gestion des utilisateurs
     */
    public function testManageUserPageIsUp()
    {
        $this->logIn(UserHelper::ROLE_ADMIN);
        $this->client->request('GET', '/admin/user/list');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );

        $this->logIn(UserHelper::ROLE_AGENT);
        $this->client->request('GET', '/admin/user/list');

        static::assertSame(
            Response::HTTP_FORBIDDEN,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page gestion des instructeurs
     */
    public function testManageInstructorsPageIsUp()
    {
        $this->logIn(UserHelper::ROLE_ADMIN);
        $this->client->request('GET', '/admin/instructor/list');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );

        $this->logIn(UserHelper::ROLE_AGENT);
        $this->client->request('GET', '/admin/instructor/list');

        static::assertSame(
            Response::HTTP_FORBIDDEN,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page de recherche
     */
    public function testSearchPageIsUp()
    {
        $this->client->request('GET', '/search');

        static::assertSame(
            Response::HTTP_FOUND,
            $this->client->getResponse()->getStatusCode()
        );

        $this->logIn(UserHelper::ROLE_ADMIN);
        $this->client->request('GET', '/search');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page des logs de recherche
     */
    public function testSearchLogsPageIsUp()
    {
        $this->client->request('GET', '/search_logs');

        static::assertSame(
            Response::HTTP_FOUND,
            $this->client->getResponse()->getStatusCode()
        );

        $this->logIn(UserHelper::ROLE_ADMIN);
        $this->client->request('GET', '/search_logs');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Teste affichage page des statistiques
     */
    public function testStatsPageIsUp()
    {
        $this->logIn(UserHelper::ROLE_NOTARY);
        $this->client->request('GET', '/stats');

        static::assertSame(
            Response::HTTP_FORBIDDEN,
            $this->client->getResponse()->getStatusCode()
        );

        $this->logIn(UserHelper::ROLE_AGENT);
        $this->client->request('GET', '/stats');

        static::assertSame(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );
    }

    /**
     * Simule une connexion utilisateur
     *
     * @param string $role role à simuler
     *
     * @return \Symfony\Bundle\FrameworkBundle\KernelBrowser|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function logIn(string $role)
    {
        $session = self::$container->get('session');

        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->findOneByRole($role);

        $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);

        return $this->client;
    }
}
