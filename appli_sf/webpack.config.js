var Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/default')
    .setPublicPath('/build/default')
    .addEntry('app', './assets/default/js/app.js')
    .addEntry('userAdd', './assets/default/js/userAdd.js')
    .addEntry('userList', './assets/default/js/userList.js')
    .addEntry('search', './assets/default/js/search.js')
    .addEntry('letter', './assets/default/js/letter.js')
    .addEntry('stats', './assets/default/js/stats.js')
    .enableSassLoader()
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .configureBabelPresetEnv((config) => {
            config.useBuiltIns = 'usage';
            config.corejs = 3;
        }
    )

    .copyFiles(
        {
            from: './assets/default/images',
            to: 'images/[path][name].[hash:8].[ext]',
        }
    );

const defaultConfig = Encore.getWebpackConfig();
defaultConfig.name = 'default';
Encore.reset();

module.exports = [defaultConfig];
